#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include <string>

namespace njson {

enum DataType {
	TYPE_NULL,
	TYPE_TRUE,
	TYPE_FALSE,
	TYPE_INT,
	TYPE_FLOAT,
	TYPE_STRING,
	TYPE_WSTRING,
	TYPE_ARRAY,
	TYPE_OBJECT,
};

typedef std::string StringType;
typedef std::wstring WStringType;
typedef long IntType;
typedef double FloatType;

}; // end njson
