#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include <algorithm>
#include <assert.h>

namespace njson {

template <class T>
class NIndexedVector {
	char* data;
	size_t* _indexes_helper;
	size_t _capacity;
	size_t _size;
	static const size_t obj_size = sizeof(T);
public:
	NIndexedVector() : data(NULL), _indexes_helper(0), _capacity(0), _size(0) {
	};
	NIndexedVector(NIndexedVector&& other) : data(NULL), _indexes_helper(0), _capacity(0), _size(0) {
		swap(other);
	};
	NIndexedVector(const NIndexedVector& other) : data(NULL), _indexes_helper(0), _capacity(0), _size(0) {
		if(other.size()) {
			alloc(other.size());
			other.forEach([this](const T* v) {
				rawAlloc();
				new (accessAt(_size - 1)) T(*v);
			});
		}
	};
	~NIndexedVector() {
		destroy();
	};
	NIndexedVector& operator =(NIndexedVector&& other) {
		swap(other);
		other.clear();
		return *this;
	};
	NIndexedVector& operator =(const NIndexedVector& other) {
		clear();
		if(other.size()) {
			reserve(other.size());
			other.forEach([this](const T* v) {
				rawAlloc();
				new (accessAt(_size - 1)) T(*v);
			});
		}
		return *this;
	};
	void swap(NIndexedVector& other) {
		std::swap(data, other.data);
		std::swap(_indexes_helper, other._indexes_helper);
		std::swap(_capacity, other._capacity);
		std::swap(_size, other._size);
	};
	// comparators -----------------------------------
	bool operator ==(const NIndexedVector& other) const {
		if(_size != other._size) {
			return false;
		}
		if(data && other.data) {
			return other.checkAll([this](const T* v, size_t i) {
				return *accessAt(i) == *v;
			});
		}
		return true;
	};
	bool operator !=(const NIndexedVector& other) const {
		return ! operator==(other);
	};
	// size-management -----------------------------------
	void clear() {
		forEach([](T* v) { v->~T(); });
		_size = 0;
	};
	size_t size() const {
		return _size;
	};
	void reserve(size_t n) {
		if(n > _size) {
			NIndexedVector other;
			other.alloc(n);
			forEach([&other](T* v) {
				other.rawAlloc();
				new (other.accessAt(other._size - 1)) T(std::move(*v));
			});
			swap(other);
		}
	};
	// accessors -----------------------------------
	T& operator [](size_t index) {
		assert(index < _size);
		return *accessAt(index);
	};
	const T& operator [](size_t index) const {
		assert(index < _size);
		return *accessAt(index);
	};
	// appenders -----------------------------------
	void pushBack(const T& value) {
		checkAppendSize();
		rawAlloc();
		new (accessAt(_size - 1)) T(value);
	};
	void pushBack(T&& value) {
		checkAppendSize();
		rawAlloc();
		new (accessAt(_size - 1)) T(std::move(value));
	};
	template <typename... Args> void emplaceBack(Args&&... args) {
		checkAppendSize();
		rawAlloc();
		new (accessAt(_size - 1)) T(std::forward<Args>(args)...);
	};
	// iterators -----------------------------------
	class ConstIterator : public std::iterator<std::random_access_iterator_tag, T> {
	protected:
		union {
			NIndexedVector* owner;
			const NIndexedVector* c_owner;
		} u;
		size_t i;
		ConstIterator(NIndexedVector* v, size_t pos) : i(pos) { u.owner = v; };
	public:
		ConstIterator(const NIndexedVector* v = NULL, size_t pos = 0) : i(pos) { u.c_owner = v; };

		bool operator ==(const ConstIterator& other) const { assert(u.c_owner == other.u.c_owner); return i == other.i; };
		bool operator !=(const ConstIterator& other) const { return ! operator==(other); };
		bool operator <(const ConstIterator& other) const { assert(u.c_owner == other.u.c_owner); return i < other.i; };

		const T& operator *() const { assert(hasValue()); return *(u.c_owner->accessAt(i)); };
		const T* operator ->() const { assert(hasValue()); return u.c_owner->accessAt(i); };

		ConstIterator& operator ++() { ++i; return *this; };
		ConstIterator operator ++(int) { auto res = *this; ++i; return res; };

		ConstIterator operator +(size_t dist) const { return ConstIterator(u.c_owner, i + dist); };
		ConstIterator operator -(size_t dist) const { return ConstIterator(u.c_owner, i - dist); };

		ConstIterator& operator +=(size_t dist) { i += dist; return *this; };
		ConstIterator& operator -=(size_t dist) { i -= dist; return *this; };

		size_t operator -(const ConstIterator& right) const { assert(u.c_owner == right.u.c_owner); return i - right.i; };

		bool isValid() const { assert(u.c_owner != NULL); return i <= u.c_owner->size(); };
		bool hasValue() const { assert(u.c_owner != NULL); return i < u.c_owner->size(); };
		size_t getPos() const { return i; };
		const NIndexedVector* getOwner() const { return u.c_owner; };
	};
	class Iterator : public ConstIterator {
	public:
		Iterator(NIndexedVector* v = NULL, size_t pos = 0) : ConstIterator(v, pos) {};

		T& operator *() { assert(this->hasValue()); return *(this->u.owner->accessAt(this->i)); };
		T* operator ->() const { assert(this->hasValue()); return this->u.owner->accessAt(this->i); };

		Iterator& operator ++() { ++(this->i); return *this; };
		Iterator operator ++(int) { auto res = *this; ++(this->i); return res; };

		Iterator operator +(size_t dist) const { return Iterator(this->u.owner, this->i + dist); };
		Iterator operator -(size_t dist) const { return Iterator(this->u.owner, this->i - dist); };

		Iterator& operator +=(size_t dist) { this->i += dist; return *this; };
		Iterator& operator -=(size_t dist) { this->i -= dist; return *this; };

		size_t operator -(const Iterator& right) const { assert(this->u.owner == right.u.owner); return this->i - right.i; };
	};
	// -----------------------------------

	Iterator begin() { return Iterator(this, 0); };
	ConstIterator begin() const { return ConstIterator(this, 0); };
	Iterator end() { return Iterator(this, _size); };
	ConstIterator end() const { return ConstIterator(this, _size); };

	// insertions -----------------------------------
	template <typename... Args> void emplace(const ConstIterator& iter, Args&&... args) {
		assert(iter.isValid() && (iter.getOwner() == this));
		if(! iter.isValid()) {
			return;
		}
		checkAppendSize();
		insertAt(iter.getPos());
		new (accessAt(iter.getPos())) T(std::forward<Args>(args)...);
	};
	void insert(const ConstIterator& iter, T&& v) {
		assert(iter.isValid() && (iter.getOwner() == this));
		if(! iter.isValid()) {
			return;
		}
		checkAppendSize();
		insertAt(iter.getPos());
		new (accessAt(iter.getPos())) T(std::move(v));
	};
	void insert(const ConstIterator& iter, const T& v) {
		assert(iter.isValid() && (iter.getOwner() == this));
		if(! iter.isValid()) {
			return;
		}
		checkAppendSize();
		insertAt(iter.getPos());
		new (accessAt(iter.getPos())) T(v);
	};
	// removers -----------------------------------
	void erase(const ConstIterator& iter, bool till_end = false) {
		assert(iter.isValid() && (iter.getOwner() == this));
		if(iter.hasValue()) {
			if(! till_end) {
				accessAt(iter.getPos())->~T();
				eraseAt(iter.getPos());
			} else {
				auto cnt = _size - iter.getPos();
				for(size_t index = 0; index < cnt; ++index) {
					accessAt(iter.getPos())->~T();
					eraseAt(iter.getPos());
				}
			}
		}
	};
private:
	template <class Func> void forEach(Func func) {
		for(size_t i = 0; i < _size; ++i) {
			func(accessAt(i));
		}
	};
	template <class Func> void forEach(Func func) const {
		for(size_t i = 0; i < _size; ++i) {
			func(accessAt(i));
		}
	};
	template <class Func> bool checkAll(Func func) const {
		for(size_t i = 0; i < _size; ++i) {
			if(! func(accessAt(i), i)) {
				return false;
			}
		}
		return true;
	};
	void alloc(size_t cnt) {
		auto total = cnt * (obj_size + sizeof(size_t));
		data = new char[total];
		_indexes_helper = static_cast<size_t*>(static_cast<void*>(data + obj_size * cnt));
		_capacity = cnt;
		_size = 0;
		for(size_t i = 0; i < _capacity; ++i) {
			_indexes_helper[i] = i;
		}
	};
	void destroy() {
		if(data) {
			clear();
			delete [] data;
			resetFields();
		}
	};
	void resetFields() {
		data = NULL;
		_indexes_helper = NULL;
		_capacity = 0;
		_size = 0;
	};
	void checkAppendSize() {
		if(_size == _capacity) {
			reserve(_size == 0 ? 8 : _size * 2);
		}
	};
	void rawAlloc() {
		++_size;
	};
	// data structure logic -----------------------------------
	T* accessAt(size_t idx) {
		return static_cast<T*>(static_cast<void*>(data + obj_size * _indexes_helper[idx]));
	};
	const T* accessAt(size_t idx) const {
		return static_cast<const T*>(static_cast<const void*>(data + obj_size * _indexes_helper[idx]));
	};
	void eraseAt(size_t idx) {
		assert(idx < _size);
		if(_size < 2) {
			_size = 0;
			return;
		}
		auto next = _indexes_helper[idx];
		for(size_t i = idx; i < _size - 1; ++i) {
			_indexes_helper[i] = _indexes_helper[i + 1];
		}
		_indexes_helper[_size - 1] = next;
		--_size;
	};
	void insertAt(size_t idx) {
		if(idx < _size) {
			auto next = _indexes_helper[_size];
			for(size_t i = _size; i > idx; --i) {
				_indexes_helper[i] = _indexes_helper[i - 1];
			}
			_indexes_helper[idx] = next;
		}
		++_size;
	};
};

}; // end njson
