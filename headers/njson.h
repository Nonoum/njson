#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "tasty_interface.h"

/*
	All functionality is implemented in class njson::Record, see "Record.h" for interface details.

	*** File "tasty_interface.h" contains comfortable definitions for creating json objects of all types.
	Example usage:
		auto my_json = njson::JObject("name", njson::JString("Vasya"),
										"age", njson::JInt(20),
										"accounts", njson::JArray(njson::JString("Petrovich")));
	All json objects has type "njson::Record" so you can always use this type directly if you don't like 'auto' for some reasons.
	Example saving:
		std::string compact_json_string = my_json.write(njson::FormatInfo::Compact());
		std::string readable_json_string = my_json.write(njson::FormatInfo::Pretty());
		std::string windows_readable_json_string = my_json.write(njson::FormatInfo::PrettyCRLF());

	'Record::write' method has parameter of type 'njson::FormatInfo',
	and defaulted to njson::FormatInfo::Compact() so you can omit it.

	*** File "utils.h" contains definitions of njson::FormatInfo and njson::ParsingState.

	Also you can configure njson::FormatInfo for some your purposes:
		auto my_format = njson::FormatInfo::Pretty();
		my_format.around_colon = "  "; // two spaces, for 'Pretty' this value is set to one space
		// ...
		std::string customly_formatted = my_json.write(my_format);
	Example above would give by two spaces before and after each semicolon between keys and values of objects ("key"  :  "value").

	Example reading json strings:
		auto my_json = njson::ReadJson("[1,2,3,4,5]");
		njson::ParsingState st;
		auto my_json_2 = njson::ReadJson("[1,2,3,4,5]", &st); // adding optional parameter to get information about first error
	njson::ParsingState class is described good enough in "utils.h".

	In case of parsing errors you can expect a non-null object contained all that was successfully read before that error, but optional
	parameter 'njson::ParsingState' will contain error information.

	Object Types:
	Object can't change it's type except it's one of njson::TYPE_TRUE or njson::TYPE_FALSE.
	For changing types of existed objects you can reassign them:
		auto my_int = njson::JInt(123);
		my_int.setInt(987); // OK
		my_int.setString("atata"); // FAIL, assert will be risen
		my_int = njson::JString("atata"); // OK

		auto my_bool = njson::JTrue();
		my_bool.setBool(false); // OK, but this is exceptional case. same with njson::JFalse

	Collections:
		Array's specific method 'atIndex' returns pointer to object if index is valid, or NULL in other case.
		Array's method 'remove' removes child record with specified index and returns true if index is valid, returns false otherwise.

		Object's similar method 'atKey' returns pointer to object if key is present in object's map, or NULL in other case.
		Object's method 'removeKey' removes child record with specified key and returns true if key is in object's map, returns false otherwise.

	*** Note that if some method marked as belonged to specific type (e.g. setInt belongs to TYPE_INT), then it will raise an assert on attempt
	to call it for record with any other type, or attempt to do something specifically illegal (e.g. access array's child at incorrect index),
	however it's safe in release build (internal checks are made everywhere).

	You can find more usage examples in test files (see sources in 'tests/' directory).
*/
