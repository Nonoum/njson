#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "njson_types.h"
#include "utils.h"
#include "NIndexedVector.h"

#include <vector>
#include <functional>
#include <istream>

namespace njson {

struct FormatCalculations;

/*
	A god-object that implements everything.
	There is used some hardcore stuff with 'placement new' and strict manual management for better performance.

	*** UNICODE notes:
		There's two different string types: TYPE_STRING (ASCII string) and TYPE_WSTRING (UNICODE string);
		- On parsing JSON from ANY source, resulting string will be ASCII string if it doesn't contain any UNICODE char;
		- On parsing JSON from ANY source, resulting string will be UNICODE string if it has any chars above
			ASCII's bound (127) or any escaped chars (in '\uXXXX' format);
		- On writing UNICODE string to ASCII output string will be minimized, meaning that ASCII chars
			will be represented as ASCII chars, and UNICODE chars will be converted to '\uXXXX' where XXXX is char's hex code;

		- Keys of objects CAN'T contain UNICODE characters, any UNICODE character in object's key will be replaced with '#',
			all other characters will be represented as is.
*/
class Record {
	// helper wrapper to distinguish operator[] for call 'record[0]' (MS Visual Studio).
	struct KeyType {
		const char* key;
		KeyType(const char* _key) : key(_key) {};
		operator const char*() const { return key; };
	};
	// types
	typedef std::vector<Record> ArrayType;
	typedef std::pair<std::string, Record> Pair; // can't declare nested class with 'Record' field
	class ObjectMap;
	// data
	DataType type;
	union {
		char data;
		char d_string[sizeof(StringType)];
		char d_wstring[sizeof(WStringType)];
		char d_array[sizeof(ArrayType)];
		char d_object[sizeof(NIndexedVector<int>)]; // NIndexedVector will have same size for any type
		IntType int_value;
		FloatType float_value;
	} u;
public:
	Record(DataType _type);
	Record(const Record& other);
	Record(Record&& other);
	~Record();
	Record& operator=(const Record& other);
	Record& operator=(Record&& other);

	void clear(); // clears data and resets type to TYPE_NULL
	DataType getType() const { return type; };

	// type checkers
	bool isString() const { return type == TYPE_STRING; };
	bool isWString() const { return type == TYPE_WSTRING; };
	bool isArray() const { return type == TYPE_ARRAY; };
	bool isObject() const { return type == TYPE_OBJECT; };
	bool isInt() const { return type == TYPE_INT; };
	bool isFloat() const { return type == TYPE_FLOAT; };
	bool isTrue() const { return type == TYPE_TRUE; };
	bool isFalse() const { return type == TYPE_FALSE; };
	bool isNull() const { return type == TYPE_NULL; };

	bool isBool() const { return (type == TYPE_TRUE) || (type == TYPE_FALSE); };
	bool isNumber() const { return (type == TYPE_INT) || (type == TYPE_FLOAT); };
	bool isCollection() const { return (type == TYPE_ARRAY) || (type == TYPE_OBJECT); };

	bool isNotNull() const { return ! isNull(); };
	bool isNotCollection() const { return ! isCollection(); };

	IntType asInt(IntType default_value) const { return isInt() ? getInt() : default_value; };
	FloatType asFloat(FloatType default_value) const { return isFloat() ? getFloat() : default_value; };
	FloatType asNumber(FloatType default_value) const { return isNumber() ? getNumber() : default_value; };
	bool asBool(bool default_value) const { return isBool() ? getBool() : default_value; };
	StringType asString(StringType default_value) const { return isString() ? getString() : default_value; };
	WStringType asWString(WStringType default_value) const { return isWString() ? getWString() : default_value; };

	bool operator ==(const Record& other) const;
	bool operator !=(const Record& other) const;

	// for TYPE_TRUE and TYPE_FALSE
	bool getBool() const; // returns true if it's 'true', returns false in all other cases
	void setBool(bool val); // sets boolean value if it's already 'true' or 'false'

	// for TYPE_INT
	IntType getInt() const; // returns int value, or trash if it's not an int
	void setInt(IntType val); // sets integer value if it's an integer, otherwise does nothing

	// for TYPE_FLOAT
	FloatType getFloat() const; // returns float value, or trash if it's not a float
	void setFloat(FloatType val); // sets float value if it's a float, otherwise does nothing

	// for TYPE_INT and TYPE_FLOAT
	FloatType getNumber() const; // returns float value if it's a float or an integer, otherwise returns 0
	void setNumber(FloatType val); // sets value if it's a float or an integer, otherwise does nothing

	// for TYPE_STRING
	const StringType& getString() const; // accesses the string. accesses hidden static string if it's not a string.
	template <class T> void setString(T&& val); // sets string value if it's a string, otherwise does nothing

	// for TYPE_WSTRING
	const WStringType& getWString() const; // accesses the unicode string. accesses hidden static string if it's not a string.
	template <class T> void setWString(T&& val); // sets unicode string value if it's a unicode string, otherwise does nothing

	// for TYPE_ARRAY and TYPE_OBJECT (isCollection() checker)
	size_t getItemsCount() const; // returns count of children in array or object, or zero if it's anything else
	bool isEmpty() const { return getItemsCount() == 0; };
	void reserve(size_t count); // reserves internal storage. doesn't affect logic, used for optimization only.
	void removeByType(DataType t); // removes all child records with specified type
	void removeByValue(Record record); // removes all child records that are equal to specified one

	// for TYPE_ARRAY
	Record& operator [](size_t index); // accesses child if index is correct, otherwise accesses hidden static record to not throw exceptions
	Record* atIndex(size_t index); // returns child at specified index, or NULL if index is incorrect
	const Record* atIndex(size_t index) const;
	size_t indexOf(const Record& record); // returns index of specified record, or size_t(-1) if there's no such record
	void insert(size_t index, const Record& record); // inserts record at specified index, or in the end if index is out of range
	void insert(size_t index, Record&& record);
	void pushBack(const Record& record); // appends child in the end of array
	void pushBack(Record&& record);
	bool remove(size_t index); // removes child at specified index, returns true if index was correct
	void append(const Record& other_array); // appends array with other_array's values
	void append(Record&& other_array);

	// for TYPE_OBJECT
	Record& operator [](const KeyType key); // accesses child, creates one if key was absent; accesses hidden static record if it's not an object.
	Record& get(const KeyType key, Record&& default_value); // similar to operator[key], but specifies default value for comfortable usage
	Record* atKey(const char* key); // returns child with specified key, or NULL if there's no such key
	const Record* atKey(const char* key) const;
	void assign(const char* key, Record&& value); // assigns value for key. similar to record[key] = std::move(value), but a bit more optimal
	bool contains(const char* key) const { return atKey(key) != NULL; }; // checks whether object has specified key
	bool removeKey(const char* key); // removes child with specified key, returns true if there were such one
	void merge(const Record& other_object); // merges objects, if have same keys, value for such keys will be overwritten by value of other_object
	void merge(Record&& other_object);
	void iterate(const std::function<void(const std::string& key, Record& value)>& func); // runs function for each key/value pair
	void iterate(const std::function<void(const std::string& key, const Record& value)>& func) const; // runs function for each key/value pair as constant

	// serialization
	std::string write(const FormatInfo& format = FormatInfo::Compact()) const; // writes json to std::string
	std::wstring writeW(const FormatInfo& format = FormatInfo::Compact()) const; // writes json to std::wstring
	void write(std::ostream& dst, const FormatInfo& format = FormatInfo::Compact()) const; // writes json to std::ostream
	void writeW(std::wostream& dst, const FormatInfo& format = FormatInfo::Compact()) const; // writes json to std::wostream

	static Record Parse(const std::string& json_string, ParsingState* state = NULL); // reads json from std::string
	static Record Parse(const std::wstring& json_string, ParsingState* state = NULL); // reads json from std::wstring
	static Record Parse(std::istream& input_stream, ParsingState* state = NULL); // reads json from std::istream
	static Record Parse(std::wistream& input_stream, ParsingState* state = NULL); // reads json from std::wistream

private:
	inline StringType* accessString()               { return static_cast<StringType*>      (static_cast<void*>(&u.data)); };
	inline const StringType* accessString() const   { return static_cast<const StringType*>(static_cast<const void*>(&u.data)); };
	inline WStringType* accessWString()             { return static_cast<WStringType*>      (static_cast<void*>(&u.data)); };
	inline const WStringType* accessWString() const { return static_cast<const WStringType*>(static_cast<const void*>(&u.data)); };
	inline ArrayType* accessArray()                 { return static_cast<ArrayType*>      (static_cast<void*>(&u.data)); };
	inline const ArrayType* accessArray() const     { return static_cast<const ArrayType*>(static_cast<const void*>(&u.data)); };
	inline ObjectMap* accessObjectMap()             { return static_cast<ObjectMap*>      (static_cast<void*>(&u.data)); };
	inline const ObjectMap* accessObjectMap() const { return static_cast<const ObjectMap*>(static_cast<const void*>(&u.data)); };

	void init(DataType _type);

	// template parsers
	template <class CharType, class SourceType> static Record ParseAnything(SourceType& src, ParsingState* state);
	template <class CharType, class SourceType> static Record ParseArray(SourceType& src, ParsingState* state);
	template <class CharType, class SourceType> static Record ParseObject(SourceType& src, ParsingState* state);
	template <class CharType, class StringBuilderType, class SourceType> static Record ParseString(SourceType& src, ParsingState* state);

	template <class CharType, class SourceType> static Record ParseConstant(SourceType& src, ParsingState* state);
	template <class CharType, class SourceType> static Pair ParseKeyValue(SourceType& src, ParsingState* state);

	// writers
	size_t estimate(FormatCalculations* fmt) const; // returns roughly estimated size of object
	template <class WriterType> void writeAnything(WriterType& dst, FormatCalculations* fmt) const;
	template <class WriterType> void writeInt(WriterType& dst, FormatCalculations* fmt) const;
	template <class WriterType> void writeFloat(WriterType& dst, FormatCalculations* fmt) const;
	template <class WriterType> void writeArray(WriterType& dst, FormatCalculations* fmt) const;
	template <class WriterType> void writeObject(WriterType& dst, FormatCalculations* fmt) const;
};

template <class T>
void Record :: setString(T&& val) {
	assert(isString());
	if(isString()) {
		*accessString() = std::forward<T>(val);
	}
};

template <class T>
void Record :: setWString(T&& val) {
	assert(isWString());
	if(isWString()) {
		*accessWString() = std::forward<T>(val);
	}
};

}; // end njson
