#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "njson_types.h"

namespace njson {

enum class ParsingError {
	SUCCESS, // parsing succeeded
	NO_DATA, // data ended at some unexpected point
	BAD_CHARACTER, // unexpected character at some point
	BAD_NUMBER, // error during parsing number
	EMPTY_KEY, // empty key in object
};

/*
	Self documented state class for obtaining information about parsing result.
	In case of errors the first error is set with one of class's methods.
*/
struct ParsingState {
	ParsingError result; // first error or ParsingError::SUCCESS
	size_t position; // position of error in data stream, or zero in case of success
	DataType last_type; // type of object that was in process at moment of first error
	int character; // bad character's code, in case of ParsingError::BAD_CHARACTER

	ParsingState() : result(ParsingError::SUCCESS), position(0), last_type(TYPE_NULL), character(0) {};

	void setNoData(size_t pos, DataType type) {
		result = ParsingError::NO_DATA;
		position = pos;
		last_type = type;
	};
	void setBadChar(size_t pos, int ch, DataType type) {
		result = ParsingError::BAD_CHARACTER;
		position = pos;
		character = ch;
		last_type = type;
	};
	void setBadNumber(size_t pos) {
		result = ParsingError::BAD_NUMBER;
		position = pos;
	};
	void setEmptyKey(size_t pos) {
		result = ParsingError::EMPTY_KEY;
		position = pos;
		last_type = TYPE_OBJECT;
	};
	bool succeeded() const {
		return result == ParsingError::SUCCESS;
	};
	bool failed() const {
		return ! succeeded();
	};
};

/*
	Struct for formatting output json.
*/
struct FormatInfo {
	const char* after_block_open; // [<after_block_open>1,2,3,4]
	const char* after_comma; // [1,<after_comma>,2<after_comma>]
	const char* after_value; // [1,<after_comma><after_value>,2<after_comma><after_value>]
	const char* around_colon; // "my_key"<around_colon>:<around_colon>my_value
	const char* pre_indent; // [<pre_indent>1,<pre_indent>2]

	FormatInfo() : after_block_open(NULL), after_comma(NULL), after_value(NULL), around_colon(NULL), pre_indent(NULL) {};

	static FormatInfo Compact() { return FormatInfo(); };
	static FormatInfo Pretty() {
		FormatInfo f;
		f.after_block_open = "\n";
		f.after_value = "\n";
		f.around_colon = " ";
		f.pre_indent = "\t";
		return f;
	};
	static FormatInfo PrettyCRLF() {
		FormatInfo f;
		f.after_block_open = "\r\n";
		f.after_value = "\r\n";
		f.around_colon = " ";
		f.pre_indent = "\t";
		return f;
	};
};

}; // end njson
