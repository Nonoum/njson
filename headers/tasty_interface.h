#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "Record.h"

namespace njson {

// parsing

inline Record ReadJson(const std::string& json_string, ParsingState* state = NULL) {
	return Record::Parse(json_string, state);
};

inline Record ReadJson(const std::wstring& json_string, ParsingState* state = NULL) {
	return Record::Parse(json_string, state);
};

inline Record ReadJson(std::istream& json_stream, ParsingState* state = NULL) {
	return Record::Parse(json_stream, state);
};

inline Record ReadJson(std::wistream& json_stream, ParsingState* state = NULL) {
	return Record::Parse(json_stream, state);
};

// arrays

inline void ArrayPushRecordsHelper(Record* rec) {};

template <class T, class... Args> inline void ArrayPushRecordsHelper(Record* rec, T&& argument, Args&&... args) {
	rec->pushBack(std::forward<T>(argument));
	ArrayPushRecordsHelper(rec, std::forward<Args>(args)...);
};

template <class... Args> inline Record JArray(Args&&... args) {
	Record r(TYPE_ARRAY);
	r.reserve(sizeof...(args));
	ArrayPushRecordsHelper(&r, std::forward<Args>(args)...);
	return r;
};

// objects

inline void ObjectPushRecordsHelper(Record* rec) {};

template <class T, class... Args> inline void ObjectPushRecordsHelper(Record* rec, const char* key, T&& value, Args&&... args) {
	rec->assign(key, std::forward<T>(value));
	ObjectPushRecordsHelper(rec, std::forward<Args>(args)...);
};

template <class... Args> inline Record JObject(Args&&... args) {
	Record r(TYPE_OBJECT);
	r.reserve(sizeof...(args) / 2);
	ObjectPushRecordsHelper(&r, std::forward<Args>(args)...);
	return r;
};

// strings

inline Record JString() {
	return Record(TYPE_STRING);
};

template <class T> Record JString(T&& value) {
	Record r(TYPE_STRING);
	r.setString(std::forward<T>(value));
	return r;
};

inline Record JWString() {
	return Record(TYPE_WSTRING);
};

template <class T> Record JWString(T&& value) {
	Record r(TYPE_WSTRING);
	r.setWString(std::forward<T>(value));
	return r;
};

// numbers

inline Record JInt(IntType value = 0) {
	Record r(TYPE_INT);
	r.setInt(value);
	return r;
};

inline Record JFloat(FloatType value = 0) {
	Record r(TYPE_FLOAT);
	r.setFloat(value);
	return r;
};

// constants

inline Record JBool(bool value) {
	return Record(value ? TYPE_TRUE : TYPE_FALSE);
};

inline Record JTrue() {
	return Record(TYPE_TRUE);
};

inline Record JFalse() {
	return Record(TYPE_FALSE);
};

inline Record JNull() {
	return Record(TYPE_NULL);
};

}; // end njson
