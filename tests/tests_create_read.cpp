/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/Record.h"

using namespace njson;

NTEST(test_create_bool_set_get) {
	Record r(TYPE_TRUE);
	r.setBool(false);
	approve(r.getBool() == false);
	r.setBool(true);
	approve(r.getBool() == true);
};

NTEST(test_create_int_set_get) {
	Record r(TYPE_INT);
	r.setInt(123);
	approve(r.getInt() == 123);
	r.setInt(456);
	approve(r.getInt() == 456);
};

NTEST(test_create_float_set_get) {
	Record r(TYPE_FLOAT);
	r.setFloat(123);
	approve((r.getFloat() > 122) && (r.getFloat() < 124));
	r.setFloat(456);
	approve((r.getFloat() > 455) && (r.getFloat() < 457));
};

NTEST(test_create_number_set_get) {
	for(auto type : {TYPE_INT, TYPE_FLOAT}) {
		Record r(type);
		r.setNumber(123);
		approve((r.getNumber() > 122) && (r.getNumber() < 124));
		r.setNumber(456);
		approve((r.getNumber() > 455) && (r.getNumber() < 457));
	}
};

NTEST(test_create_string_set_get) {
	Record r(TYPE_STRING);
	r.setString("zxcvbnm asdf");
	approve(r.getString() == "zxcvbnm asdf");
};

NTEST(test_create_wstring_set_get) {
	Record r(TYPE_WSTRING);
	r.setWString(L"zxcvbnm asdf");
	approve(r.getWString() == L"zxcvbnm asdf");
	WStringType wstr;
	wstr.push_back(0x1E22);
	wstr.push_back(0x1E14);
	wstr.push_back(0x1E38);
	wstr.push_back(0x1E38);
	wstr.push_back(0x1E4C);
	r.setWString(wstr);
	approve(r.getWString() == wstr);
	approve(r.getWString().length() == 5);
	approve(r.getWString()[0] == 0x1E22);
};

NTEST(test_create_array_get_count) {
	Record r(TYPE_ARRAY);
	Record r1(TYPE_INT);
	Record r2(TYPE_FLOAT);
	approve(r.getItemsCount() == 0);
	r.pushBack(r1);
	approve(r.getItemsCount() == 1);
	r.pushBack(r1);
	approve(r.getItemsCount() == 2);
	r.pushBack(r2);
	approve(r.getItemsCount() == 3);
};

NTEST(test_create_array_access_values) {
	Record r(TYPE_ARRAY);
	r.pushBack(Record(TYPE_INT));
	r.pushBack(Record(TYPE_STRING));
	r.pushBack(Record(TYPE_FLOAT));
	approve(r[0].isInt());
	approve(r[1].isString());
	approve(r[2].isFloat());
	r[1].setString("12345");
	approve(r[1].getString() == "12345");
};

NTEST(test_create_array_atIndex) {
	Record r(TYPE_ARRAY);
	r.pushBack(Record(TYPE_INT));
	r.pushBack(Record(TYPE_STRING));
	approve(r.atIndex(0)->isInt());
	approve(r.atIndex(1)->isString());
	approve(r.atIndex(-1) == NULL);
	approve(r.atIndex(2) == NULL);
	approve(r.atIndex(123) == NULL);
};

NTEST(test_create_object_get_count) {
	Record r(TYPE_OBJECT);
	approve(r.getItemsCount() == 0);
	r["my key"];
	approve(r.getItemsCount() == 1);
	r["my key"] = Record(TYPE_STRING);
	approve(r.getItemsCount() == 1);
	r["my key 2"] = Record(TYPE_INT);
	approve(r.getItemsCount() == 2);
};

NTEST(test_create_object_access_values) {
	Record r(TYPE_OBJECT);
	r["my key"] = Record(TYPE_STRING);
	approve(r["my key"].getString() == "");
	r["my key"].setString("09876");
	approve(r["my key"].getString() == "09876");
	r["int"] = Record(TYPE_INT);
	r["int"].setInt(-5);
	approve(r["int"].getInt() == -5);
	approve(r["my key"].getString() == "09876");
};

NTEST(test_create_object_atKey) {
	Record r(TYPE_OBJECT);
	r["int"] = Record(TYPE_INT);
	r["float"] = Record(TYPE_FLOAT);
	r["string"] = Record(TYPE_STRING);
	approve(r.atKey("string")->isString());
	approve(r.atKey("float")->isFloat());
	approve(r.atKey("int")->isInt());
	approve(r.atKey("object") == NULL);
};
