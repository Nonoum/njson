/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/NIndexedVector.h"

using namespace njson;

NTEST(test_NIndexedVector_push_read) {
	const size_t vals[] = {100,200,500,300,999,400,200,777,2345,3,2516,213,83,2,19,4853,435,325,55,5555,55555,5};
	NIndexedVector<size_t> arr;
	const size_t size = sizeof(vals) / sizeof(*vals);
	for(size_t end = 0; end < size; ++end) {
		arr.pushBack(vals[end]);
		approve(arr.size() == end + 1);
		for(size_t i = 0; i <= end; ++i) {
			approve(arr[i] == vals[i]);
		}
		size_t i = 0;
		for(auto value : arr) {
			approve(value == vals[i]);
			++i;
		}
		approve(i == end + 1);
	}
};

NTEST(test_NIndexedVector_equality) {
	const size_t vals[] = {100,200,500,300,999,400,200,777,2345,3,2516,213,83,2,19,4853,435,325,55,5555,55555,5};
	NIndexedVector<size_t> arr1, arr2;
	const size_t size = sizeof(vals) / sizeof(*vals);
	for(size_t end = 0; end < size; ++end) {
		arr1.pushBack(vals[end]);
		arr2.pushBack(vals[end]);
		approve((arr1 == arr2) && (! (arr1 != arr2)));
	}
};

NTEST(test_NIndexedVector_copy) {
	const size_t vals[] = {100,200,500,300,999,400,200,777,2345,3,2516,213,83,2,19,4853,435,325,55,5555,55555,5};
	NIndexedVector<size_t> arr1, arr2;
	for(auto val : vals) {
		arr1.pushBack(val);
	}
	arr2 = arr1;
	approve(arr1.size() == arr2.size());
	approve((arr1 == arr2) && (! (arr1 != arr2)));
	for(auto& val : arr2) {
		auto backup = val;
		val = backup + 1;
		approve((arr1 != arr2) && (! (arr1 == arr2)));
		val = backup;
		approve((arr1 == arr2) && (! (arr1 != arr2)));
	}
};

NTEST(test_NIndexedVector_insert) {
	for(size_t cnt = 0; cnt < 10; ++cnt) {
		for(size_t pos = 0; pos < cnt; ++pos) {
			NIndexedVector<size_t> arr;
			for(size_t i = 0; i < cnt; ++i) {
				arr.pushBack(i);
			}
			arr.insert(arr.begin() + pos, 999);
			approve(arr.size() == cnt + 1);
			approve(arr[pos] == 999);
			approve(arr[pos+1] == pos);
			if(pos > 0) {
				approve(arr[pos-1] == pos-1);
			}
			size_t i = 0;
			for(auto v : arr) {
				auto expected = (i < pos) ? i : (i == pos ? 999 : i - 1);
				approve(v == expected);
				++i;
			}
			approve(i == arr.size());
		}
	}
};

NTEST(test_NIndexedVector_insert_end) {
	NIndexedVector<size_t> arr;
	arr.insert(arr.end(), 5);
	approve((arr.size() == 1) && (arr[0] == 5));
	arr.insert(arr.end(), 7);
	approve((arr.size() == 2) && (arr[0] == 5) && (arr[1] == 7));
};

NTEST(test_NIndexedVector_erase) {
	const size_t cnt = 10;
	for(size_t pos = 0; pos <= cnt; ++pos) {
		NIndexedVector<size_t> arr;
		for(size_t i = 0; i < cnt; ++i) {
			arr.pushBack(i);
		}
		arr.erase(arr.begin() + pos);
		if(pos != cnt) {
			approve(arr.size() == cnt - 1);
		}
		if(pos < cnt - 1) {
			approve(arr[pos] == pos + 1);
		}
		if(pos > 0) {
			approve(arr[pos-1] == pos - 1);
		}
	}
	for(size_t pos = 0; pos <= cnt; ++pos) {
		NIndexedVector<size_t> arr;
		for(size_t i = 0; i < cnt; ++i) {
			arr.pushBack(i);
		}
		arr.erase(arr.begin() + pos, true);
		approve(arr.size() == pos);
	}
};

NTEST(test_NIndexedVector_iterator_consistency) {
	NIndexedVector<size_t> arr;
	const size_t cnt = 10;
	for(size_t i = 0; i < cnt; ++i) {
		arr.pushBack(i);
	}
	approve(*arr.begin() == 0);
	for(size_t pos = 0; pos < cnt; ++pos) {
		auto iter = arr.begin();
		approve(*(iter + pos) == pos);
		iter += pos;
		approve(*iter == pos);
	}
};

class ReferenceCounter {
	int* counter;
public:
	ReferenceCounter(int* _counter) : counter(_counter) {
		++(*counter);
	};
	ReferenceCounter(const ReferenceCounter& other) : counter(other.counter) {
		++(*counter);
	};
	ReferenceCounter(ReferenceCounter&& other) : counter(other.counter) {
		++(*counter);
	};
	~ReferenceCounter() {
		--(*counter);
	};
};

NTEST(test_NIndexedVector_no_leaks_on_create_destroy) {
	int counter = 0;
	{
		NIndexedVector<ReferenceCounter> arr;
		for(size_t i = 0; i < 1000; ++i) {
			arr.pushBack(ReferenceCounter(&counter));
		}
	}
	approve(counter == 0);
};

NTEST(test_NIndexedVector_no_leaks_on_move) {
	int counter = 0;
	{
		NIndexedVector<ReferenceCounter> arr, other;
		for(size_t i = 0; i < 1000; ++i) {
			arr.pushBack(ReferenceCounter(&counter));
		}
		other = std::move(arr);
		NIndexedVector<ReferenceCounter> third(std::move(other));
	}
	approve(counter == 0);
};

NTEST(test_NIndexedVector_no_leaks_on_copy) {
	int counter = 0;
	{
		NIndexedVector<ReferenceCounter> arr, other;
		for(size_t i = 0; i < 1000; ++i) {
			arr.pushBack(ReferenceCounter(&counter));
		}
		other = arr;
		NIndexedVector<ReferenceCounter> third(other);
	}
	approve(counter == 0);
};

NTEST(test_NIndexedVector_no_leaks_on_insert_erase) {
	int counter = 0;
	{
		NIndexedVector<ReferenceCounter> arr;
		for(size_t i = 0; i < 10; ++i) {
			arr.pushBack(ReferenceCounter(&counter));
		}
		arr.insert(arr.begin() + 5, ReferenceCounter(&counter));
		arr.insert(arr.begin(), ReferenceCounter(&counter));
		arr.insert(arr.end(), ReferenceCounter(&counter));
		arr.erase(arr.begin() + 8);
		arr.erase(arr.begin());
		arr.erase(arr.end() - 1);
	}
	approve(counter == 0);
};

NTEST(test_NIndexedVector_no_leaks_on_emplace_operations) {
	int counter = 0;
	{
		NIndexedVector<ReferenceCounter> arr;
		for(size_t i = 0; i < 10; ++i) {
			arr.emplaceBack(&counter);
			arr.emplace(arr.begin(), &counter);
		}
	}
	approve(counter == 0);
};
