/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/tasty_interface.h"

#include <sstream>
#include <fstream>

using namespace njson;

static bool WriteReadCompare(const Record& rec) {
	const auto formats = {FormatInfo::Compact(), FormatInfo::Pretty(), FormatInfo::PrettyCRLF()};
	bool result = true;
	for(auto& f : formats) { // std::string interface
		const std::string json_string = rec.write(f);
		ParsingState st;
		const Record parsed = Record::Parse(json_string, &st);
		result &= (st.succeeded() && (rec == parsed));
	}
	for(auto& f : formats) { // std::wstring interface
		const std::wstring json_wstring = rec.writeW(f);
		ParsingState st;
		const Record parsed = Record::Parse(json_wstring, &st);
		result &= (st.succeeded() && (rec == parsed));
	}
	for(auto& f : formats) { // std::i/o stream interface
		std::ostringstream oss;
		rec.write(oss, f);
		ParsingState st;
		std::istringstream iss(oss.str());
		const Record parsed = Record::Parse(iss, &st);
		result &= (st.succeeded() && (rec == parsed));
	}
	for(auto& f : formats) { // std::w i/o stream interface
		std::wostringstream woss;
		rec.writeW(woss, f);
		ParsingState st;
		std::wistringstream wiss(woss.str());
		const Record parsed = Record::Parse(wiss, &st);
		result &= (st.succeeded() && (rec == parsed));
	}
	return result;
};

NTEST(test_write_read_compare_int) {
	approve(WriteReadCompare(JInt(0)));
	approve(WriteReadCompare(JInt(-218)));
	approve(WriteReadCompare(JInt(258697)));
	approve(WriteReadCompare(JInt(3459)));
};

NTEST(test_write_read_compare_float) {
	approve(WriteReadCompare(JFloat(0)));
	approve(WriteReadCompare(JFloat(-218)));
	approve(WriteReadCompare(JFloat(258697)));
	approve(WriteReadCompare(JFloat(3459)));
};

NTEST(test_write_read_compare_string) {
	approve(WriteReadCompare(JString("\"")));
	approve(WriteReadCompare(JString("zxc\"vbnm")));
	approve(WriteReadCompare(JString("echjb\ngf\rscfgb\txzyf")));
	approve(WriteReadCompare(JString(" sample  spaced strin g\t  ")));
};

NTEST(test_write_read_compare_wstring) {
	WStringType wstr;
	wstr.push_back(0x1E22);
	wstr.push_back(0x1E14);
	wstr.push_back(0x1E38);
	wstr.push_back(0x1E38);
	wstr.push_back(0x1E4C);
	approve(WriteReadCompare(JWString(wstr)));
};

NTEST(test_write_read_compare_array) {
	Record r(TYPE_ARRAY);
	approve(WriteReadCompare(r));
	r.pushBack( JTrue() );
	approve(WriteReadCompare(r));
	r.pushBack( JFalse() );
	approve(WriteReadCompare(r));
	r.pushBack(JInt(1234567));
	approve(WriteReadCompare(r));
};

NTEST(test_write_read_compare_array_complex) {
	auto r = JArray(JString("sample "), JString(" string"), JArray(JString("qwe")));
	approve(WriteReadCompare(r));
};

NTEST(test_write_read_compare_object) {
	auto r = JObject();
	approve(WriteReadCompare(r));
	r["t"] = JTrue();
	approve(WriteReadCompare(r));
	r["f"] = JFalse();
	approve(WriteReadCompare(r));
	r["i"] = JInt(1234567);
	approve(WriteReadCompare(r));
};

NTEST(test_write_read_compare_object_complex) {
	auto r = JObject("nested", JObject("is true", JTrue(), "is null", JNull(), "string", JString("some string")),
						"string", JString("other string"),
						"int", JInt(0x22448800));
	approve(WriteReadCompare(r));
};

NTEST(test_write_read_compare_object_complex_wstrings) {
	WStringType wstr1, wstr2, wstr3, wstr4;
	wstr1.push_back(0x1E22);
	wstr1.push_back(0x1E14);
	wstr1.push_back(0x1E38);
	wstr2 = wstr1;
	wstr1.push_back(0x1E38);
	wstr1.push_back(0x1E4C);
	wstr3 = L"ascii";
	wstr3.append(wstr1);
	wstr4 = wstr1;
	wstr4.append(L"ascii");
	auto r = JObject("nested", JObject("is true", JTrue(), "is null", JNull(), "wstr1", JWString(wstr1), "string", JString("some string")),
						"string", JString("other string"), "wstr2", JWString(wstr2), "wstr1", JWString(wstr1), "wstr3", JWString(wstr3),
						"int", JInt(0x22448800), "arr", JArray(JWString(wstr4), JWString(wstr1)));
	approve(WriteReadCompare(r));
};

NTEST(test_load_file_content) {
	WStringType wstr1, wstr2, wstr3, wstr4;
	wstr1.push_back(0x1E22);
	wstr1.push_back(0x1E14);
	wstr1.push_back(0x1E38);
	wstr2 = wstr1;
	wstr1.push_back(0x1E38);
	wstr1.push_back(0x1E4C);
	wstr3 = L"ascii";
	wstr3.append(wstr1);
	wstr4 = wstr1;
	wstr4.append(L"ascii");
	auto r = JObject("nested", JObject("is true", JTrue(), "is null", JNull(), "wstr1", JWString(wstr1), "string", JString("some string")),
						"string", JString("other string"), "wstr2", JWString(wstr2), "wstr1", JWString(wstr1), "wstr3", JWString(wstr3),
						"int", JInt(0x22448800), "arr", JArray(JWString(wstr4), JWString(wstr1)));

	auto read_ansi_file = [](const char* fname) {
		std::ifstream ifs(fname, std::ios::binary);
		return Record::Parse(ifs);
	};
	auto write_ansi_file = [](const char* fname, const Record& r) {
		std::ofstream ofs(fname, std::ios::binary);
		r.write(ofs);
	};
	(void)write_ansi_file;
	approve(r == read_ansi_file("../../tests/data/json_1_ascii.bin"));
	for(int i = 0; i < 30; ++i) {
		r.assign("recursive", Record(r));
	}
	approve(r == read_ansi_file("../../tests/data/json_2_ascii.bin"));
};

NTEST(test_write_read_compare_complex) {
	Record arr1(TYPE_ARRAY), arr2(TYPE_ARRAY);
	Record obj1(TYPE_OBJECT), obj2(TYPE_OBJECT);
	Record rtrue(TYPE_TRUE), rfalse(TYPE_FALSE), rnull(TYPE_NULL);

	Record rhello(TYPE_STRING), rworld(TYPE_STRING);
	rhello.setString("Hello");
	rworld.setString("World");

	Record int100(TYPE_INT), int200(TYPE_INT), int333(TYPE_INT);
	int100.setInt(100);
	int200.setInt(200);
	int333.setInt(333);

	arr1.pushBack(int100);
	arr1.pushBack(int200);
	obj1["some"] = rhello;
	obj1["thing"] = rtrue;
	obj1["else"] = rnull;
	arr1.pushBack(obj1);

	arr2.pushBack(int100);
	arr2.pushBack(int333);
	arr2.pushBack(rfalse);
	obj2["key"] = arr2;
	obj2["key2"] = rworld;

	Record root1(TYPE_ARRAY), root2(TYPE_OBJECT);
	root1.pushBack(arr1);
	root1.pushBack(obj2);
	approve(WriteReadCompare(root1));

	root2["root"] = obj2;
	root2["something"] = arr1;
	approve(WriteReadCompare(root2));

	root1.pushBack(root2);
	approve(WriteReadCompare(root1));

	root2["recursive"] = root1;
	approve(WriteReadCompare(root2));
};
