/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/Record.h"

#include <sstream>

using namespace njson;

NTEST(test_parse_int) {
	auto check_ints_equal = [](const char* json_string, IntType val) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && (r.getInt() == val);
	};
	approve(check_ints_equal("1234", 1234));
	approve(check_ints_equal("0", 0));
	approve(check_ints_equal("0000", 0));
	approve(check_ints_equal("-0", 0));
	approve(check_ints_equal("-17", -17));
	approve(check_ints_equal("+95", 95));
	approve(check_ints_equal("123qwe", 123));
};

NTEST(test_parse_float) {
	auto check_floats_equal = [](const char* json_string, FloatType val) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		const auto diff = r.getFloat() - val;
		return st.succeeded() && (diff > -0.01) && (diff < 0.01);
	};
	approve(check_floats_equal("4.567", 4.567));
	approve(check_floats_equal("-56.789", -56.789));
	approve(check_floats_equal("+98.77", 98.77));
	approve(check_floats_equal("56.7asd", 56.7));
	// scientific
	approve(check_floats_equal("56.1E+2", 5610));
	approve(check_floats_equal("56.1E-1", 5.61));
	approve(check_floats_equal("56E-1", 5.6));
	approve(check_floats_equal("56E+3", 56000));
};

NTEST(test_parse_true) {
	auto is_true = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isTrue();
	};
	approve(is_true("true"));
	approve(is_true(" true"));
	approve(is_true("true "));
	approve(is_true("  \rtrue  \t\n"));
};

NTEST(test_parse_false) {
	auto is_false = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isFalse();
	};
	approve(is_false("false"));
	approve(is_false(" false"));
	approve(is_false("false "));
	approve(is_false("  \rfalse  \t\n"));
};

NTEST(test_parse_null) {
	auto is_null = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isNull();
	};
	approve(is_null("null"));
	approve(is_null(" null"));
	approve(is_null("null "));
	approve(is_null("  \rnull  \t\n"));
};

NTEST(test_parse_array_empty) {
	auto is_empty_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isArray() && (r.getItemsCount() == 0);
	};
	approve(is_empty_array("[]"));
	approve(is_empty_array("[ ]"));
	approve(is_empty_array("[   ]"));
	approve(is_empty_array("[ \n]"));
	approve(is_empty_array("[\r]"));
	approve(is_empty_array("[ \r \n\t ]"));
};

NTEST(test_parse_array_single_value) {
	auto is_single_valued_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isArray() && (r.getItemsCount() == 1) && (! r[0].isNull());
	};
	approve(is_single_valued_array("[true]"));
	approve(is_single_valued_array("[ true]"));
	approve(is_single_valued_array("[true ]"));
	approve(is_single_valued_array("[ \t\rtrue\n ]"));
	approve(is_single_valued_array("[ 5]"));
	approve(is_single_valued_array("[-300]"));
	approve(is_single_valued_array("[false\n\n\t] \t"));
};

NTEST(test_parse_array_two_values) {
	auto is_two_valued_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isArray() && (r.getItemsCount() == 2) && r[0].isTrue() && r[1].isFalse();
	};
	approve(is_two_valued_array("[true,false]"));
	approve(is_two_valued_array("[true \t,false]"));
	approve(is_two_valued_array("[true,\t false]"));
	approve(is_two_valued_array("[true\r  , \t false]"));
	approve(is_two_valued_array("[  true\r  , \t false  \n] \r"));
};

NTEST(test_parse_array_nested) {
	auto has_nested_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isArray() && (r.getItemsCount() >= 1) && r[0].isArray();
	};
	approve(has_nested_array("[[]]"));
	approve(has_nested_array("[[ ]]"));
	approve(has_nested_array("[ [ ]]"));
	approve(has_nested_array("[[ ] ]"));
	approve(has_nested_array("[ [ ] ]"));
	approve(has_nested_array("[[ ], false]"));
	approve(has_nested_array("[[true], false]"));
};

NTEST(test_parse_object_empty) {
	auto is_empty_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isObject() && (r.getItemsCount() == 0);
	};
	approve(is_empty_object("{}"));
	approve(is_empty_object("{ }"));
	approve(is_empty_object("{   }"));
	approve(is_empty_object("{ \n}"));
	approve(is_empty_object("{\r}"));
	approve(is_empty_object("{ \r \n\t }"));
};

NTEST(test_parse_object_single_pair) {
	auto is_single_paired_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isObject() && (r.getItemsCount() == 1)
				&& r.atKey("qwe") && r["qwe"].isFalse();
	};
	approve(is_single_paired_object("{\"qwe\":false}"));
	approve(is_single_paired_object("{\"qwe\" \t:false}"));
	approve(is_single_paired_object("{\"qwe\":\t false}"));
	approve(is_single_paired_object("{\"qwe\" \t : \t false}"));
};

NTEST(test_parse_object_two_pairs) {
	auto is_two_paired_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isObject() && (r.getItemsCount() == 2)
				&& r.atKey("qwe") && r["qwe"].isFalse()
				&& r.atKey("asdf") && r["asdf"].isTrue();
	};
	approve(is_two_paired_object("{\"qwe\":false,\"asdf\":true}"));
	approve(is_two_paired_object("{\"qwe\":false \t,\"asdf\":true}"));
	approve(is_two_paired_object("{\"qwe\":false ,\t\"asdf\":true}"));
	approve(is_two_paired_object("{\"qwe\": false \n , \r \"asdf\" : true }"));
};

NTEST(test_parse_object_nested) {
	auto has_nested_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isObject() && (r.getItemsCount() >= 1)
				&& r.atKey("qwe") && r["qwe"].isObject();
	};
	approve(has_nested_object("{\"qwe\":{}}"));
	approve(has_nested_object("{\"qwe\":{ }}"));
	approve(has_nested_object("{\"qwe\": { }}"));
	approve(has_nested_object("{\"qwe\": { } }"));
	approve(has_nested_object("{\"qwe\": { \"1\":true} }"));
	approve(has_nested_object("{\"qwe\": { \"1\":true}, \"asdf\":false }"));
};

NTEST(test_parse_object_key_with_quote) {
	auto object_has_key = [](const char* json_string, const char* key) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isObject() && r.getItemsCount() && r.atKey(key) && r[key].isTrue();
	};
	approve(object_has_key("{\"qwe\\\"\":true}", "qwe\"")); // key is 'qwe"'
	approve(object_has_key("{\"q\\\"we\":true}", "q\"we")); // key is 'q"we'
	approve(object_has_key("{\"q\\\"w\\\"e\":true}", "q\"w\"e")); // key is 'q"w"e'
};

NTEST(test_parse_string_empty) {
	auto is_empty_string = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isString() && (r.getString().length() == 0);
	};
	approve(is_empty_string("\"\""));
	approve(is_empty_string(" \"\" "));
	approve(is_empty_string(" \n\t \"\" \r"));
};

NTEST(test_parse_string_simple) {
	auto matches_string = [](const char* json_string, const char* content) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isString() && (r.getString() == content);
	};
	approve(matches_string("\"q\"", "q"));
	approve(matches_string("  \"q\" ", "q"));
	approve(matches_string(" \t\n \"q\" ", "q"));
	approve(matches_string("\"asdfg\"", "asdfg"));
	approve(matches_string("\"sample spaced string\"", "sample spaced string"));
	approve(matches_string("\" other spaced string  \"", " other spaced string  "));
};

NTEST(test_parse_string_unicode_escaped) {
	auto matches_string = [](const char* json_string, const WStringType& wstr) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isWString() && (r.getWString() == wstr);
	};
	WStringType wstr;
	wstr.push_back('x');
	wstr.push_back(0x1E22);
	approve(matches_string("\"x\\u1E22\"", wstr));
	wstr = L"";
	wstr.push_back(0x1E22);
	approve(matches_string("\"\\u1E22\"", wstr));
	wstr.push_back(0x1E14);
	approve(matches_string("\"\\u1E22\\u1e14\"", wstr));
	wstr.append(L"LLO");
	approve(matches_string("\"\\u1E22\\u1e14LLO\"", wstr));
	wstr.push_back(0x203C);
	approve(matches_string("\"\\u1E22\\u1e14LLO\\u203C\"", wstr));
};

NTEST(test_parse_string_escaped) {
	auto matches_string = [](const char* json_string, const char* content) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.succeeded() && r.isString() && (r.getString() == content);
	};
	approve(matches_string("\"asd\\nfghj\"", "asd\nfghj"));
	approve(matches_string("\"as\\r\\tdfghj\"", "as\r\tdfghj"));
	approve(matches_string("\"asdfgh\\b\\fj\"", "asdfgh\b\fj"));
	approve(matches_string("\"a\\\\sdfg\\/hj\"", "a\\sdfg/hj"));
	approve(matches_string("\"as\\xdf\\ygh\"", "asxdfygh"));
};

NTEST(test_parse_string_wstream) {
	auto matches_string = [](std::wistream& json_stream, const char* content) {
		ParsingState st;
		Record r = Record::Parse(json_stream, &st);
		return st.succeeded() && r.isString() && (r.getString() == content);
	};
	WStringType wstr;
	wstr.append(L"\"such a simple string\"");
	std::wistringstream ss(wstr);
	approve(matches_string(ss, "such a simple string"));
};

NTEST(test_parse_string_unicode_wstream) {
	auto matches_string = [](std::wistream& json_stream, const WStringType& wstr) {
		ParsingState st;
		Record r = Record::Parse(json_stream, &st);
		return st.succeeded() && r.isWString() && (r.getWString() == wstr);
	};
	WStringType wstr;
	wstr.push_back('"');
	wstr.append(L"not a simple string");
	wstr.push_back(0x203C);
	wstr.push_back('"');
	const WStringType internal_wstr(wstr.begin() + 1, wstr.begin() + wstr.length() - 1);
	std::wistringstream ss(wstr);
	approve(matches_string(ss, internal_wstr));
};

// negative tests --------------------------

NTEST(test_neg_parse_trash) {
	auto is_trash = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed();
	};
	approve(is_trash(" "));
	approve(is_trash(" +"));
	approve(is_trash(" - "));
	approve(is_trash(" tru"));
	approve(is_trash(" fals"));
	approve(is_trash(" nul"));
	approve(is_trash(" sdvydh "));
	approve(is_trash("qwertyuiop"));
	approve(is_trash(" watyuhtn9857w hm7 wa4ytn8 {} "));
};

NTEST(test_neg_parse_string_incomplete) {
	auto is_incomplete_string = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed() && (st.last_type == TYPE_STRING) && (st.result == ParsingError::NO_DATA);
	};
	approve(is_incomplete_string("\"qwerty"));
	approve(is_incomplete_string("\"qwerty\\\""));
	approve(is_incomplete_string("\"qwerty\\\"uiop"));
};

NTEST(test_neg_parse_array_incomplete) {
	auto is_incomplete_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed() && (st.last_type == TYPE_ARRAY) && (st.result == ParsingError::NO_DATA);
	};
	approve(is_incomplete_array("["));
	approve(is_incomplete_array("[123"));
	approve(is_incomplete_array("[123 "));
	approve(is_incomplete_array("[true"));
	approve(is_incomplete_array("[true "));
	approve(is_incomplete_array("[false,"));
	approve(is_incomplete_array("[false, \n\r "));
};

NTEST(test_neg_parse_array_inconsistent) {
	auto is_inconsistent_array = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed();
	};
	approve(is_inconsistent_array("[,]"));
	approve(is_inconsistent_array("[,,]"));
	approve(is_inconsistent_array("[123,]"));
	approve(is_inconsistent_array("[123,,]"));
	approve(is_inconsistent_array("[123, , ]"));
	approve(is_inconsistent_array("[123, x]"));
	approve(is_inconsistent_array("[true false]"));
};

NTEST(test_neg_parse_object_incomplete) {
	auto is_incomplete_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed() && (st.result == ParsingError::NO_DATA)
				&& ((st.last_type == TYPE_OBJECT) || (st.last_type == TYPE_NULL));
	};
	approve(is_incomplete_object("{"));
	approve(is_incomplete_object("{\"123\""));
	approve(is_incomplete_object("{\"123\" "));
	approve(is_incomplete_object("{\"123\": "));
	approve(is_incomplete_object("{\"123\" :"));
	approve(is_incomplete_object("{\"123\" : "));
	approve(is_incomplete_object("{\"123\" : 123"));
	approve(is_incomplete_object("{\"123\" : 123 "));
	approve(is_incomplete_object("{\"123\" : 123, "));
	approve(is_incomplete_object("{\"123\" : 123, \"456"));
	approve(is_incomplete_object("{\"123\" : 123, \"456\""));
	approve(is_incomplete_object("{\"123\" : 123, \"456\": "));
	approve(is_incomplete_object("{\"123\" : 123, \"456\": 456"));
};

NTEST(test_neg_parse_object_inconsistent) {
	auto is_inconsistent_object = [](const char* json_string) {
		ParsingState st;
		Record r = Record::Parse(json_string, &st);
		return st.failed();
	};
	approve(is_inconsistent_object("{,}"));
	approve(is_inconsistent_object("{\"123\",}"));
	approve(is_inconsistent_object("{\"123\":,}"));
	approve(is_inconsistent_object("{\"123\":123,}"));
	approve(is_inconsistent_object("{\"123\":123,,}"));
	approve(is_inconsistent_object("{\"123\" : 123 : "));
	approve(is_inconsistent_object("{\"123\" : 123 456 "));
	approve(is_inconsistent_object("{\"123\" : 123 \"456 "));
	approve(is_inconsistent_object("{\"123\" : 123 \"456\" "));
	approve(is_inconsistent_object("{\"123\" : 123 \"456\", "));
	approve(is_inconsistent_object("{\"123\" : 123 \"456\" : 456, "));
};
