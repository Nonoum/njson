/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/tasty_interface.h"

using namespace njson;

NTEST(test_method_remove) {
	auto arr = JArray(JInt(123), JString("456"), JInt(789));
	auto c1 = arr, c2 = arr, c3 = arr;
	c1.remove(0);
	c2.remove(1);
	c3.remove(2);
	approve((c1.getItemsCount() == 2) && (c1[0].isString()) && (c1[1].isInt()));
	approve((c2.getItemsCount() == 2) && (c2[0].isInt()) && (c2[1].isInt()));
	approve((c3.getItemsCount() == 2) && (c3[0].isInt()) && (c3[1].isString()));
};

NTEST(test_method_removeKey) {
	auto obj = JObject("first", JInt(123), "second", JString("456"), "third", JInt(789));
	auto c1 = obj, c2 = obj, c3 = obj;
	c1.removeKey("first");
	c2.removeKey("second");
	c3.removeKey("third");
	approve((c1.getItemsCount() == 2) && (! c1.contains("first")));
	approve((c2.getItemsCount() == 2) && (! c2.contains("second")));
	approve((c3.getItemsCount() == 2) && (! c3.contains("third")));
};

NTEST(test_method_contains) {
	auto obj = JObject("first", JInt(123), "second", JString("456"), "third", JInt(789));
	approve(obj.contains("first"));
	approve(obj.contains("second"));
	approve(obj.contains("third"));
	approve(! obj.contains("fourth"));
	approve(! obj.contains("qwertyuiop"));
	approve(! obj.contains(""));
	approve(! obj.contains("ewthnu65eu"));
	approve(! obj.contains("thir"));
	approve(! obj.contains("hird"));
	approve(! obj.contains("456"));
	approve(! obj.contains("123"));
};

NTEST(test_method_iterate) {
	auto obj = JObject("first", JInt(123), "second", JString("456"), "third", JInt(789));
	size_t cnt = 0, ints_sum = 0;
	obj.iterate([&cnt, &ints_sum](const std::string& key, const Record& value) {
		++cnt;
		if(value.isInt()) {
			ints_sum += value.getInt();
		}
	});
	approve((cnt == 3) && (ints_sum == 912));
};

NTEST(test_method_indexOf) {
	auto arr = JArray(JInt(100), JInt(300), JString("asd"), JTrue(), JInt(500));
	for(size_t i = 0; i < arr.getItemsCount(); ++i) {
		approve(arr.indexOf(arr[i]) == i);
	}
	approve(arr.indexOf(JInt(100)) == 0);
	approve(arr.indexOf(JInt(500)) == 4);
	approve(arr.indexOf(JInt(300)) == 1);
	approve(arr.indexOf(JTrue()) == 3);
	approve(arr.indexOf(JString("asd")) == 2);

	approve(arr.indexOf(JString("asdf")) == size_t(-1));
	approve(arr.indexOf(JString("")) == size_t(-1));
	approve(arr.indexOf(JFalse()) == size_t(-1));
	approve(arr.indexOf(JInt(5)) == size_t(-1));
};

NTEST(test_method_insert) {
	auto arr = JArray(JInt(100), JInt(300));
	arr.insert(0, JInt(2));
	approve((arr.getItemsCount() == 3) && (arr[0] == JInt(2)));
	arr.insert(1, JInt(55));
	approve((arr.getItemsCount() == 4) && (arr[1] == JInt(55)));
	arr.insert(4, JInt(777));
	approve((arr.getItemsCount() == 5) && (arr[4] == JInt(777)));
	arr.insert(999, JInt(999));
	approve((arr.getItemsCount() == 6) && (arr[5] == JInt(999)));
};

NTEST(test_method_array_removeByType) {
	auto arr = JArray(JInt(100), JInt(300), JBool(true), JFalse(), JFloat(1), JString("111"));
	arr.removeByType(TYPE_FALSE);
	approve(arr.getItemsCount() == 5);
	arr.removeByType(TYPE_STRING);
	approve(arr.getItemsCount() == 4);
	arr.removeByType(TYPE_STRING);
	approve(arr.getItemsCount() == 4);
	arr.removeByType(TYPE_INT);
	approve(arr.getItemsCount() == 2);
	arr.removeByType(TYPE_FLOAT);
	approve(arr.getItemsCount() == 1);
	approve(arr[0].isTrue());
};

NTEST(test_method_object_removeByType) {
	auto obj = JObject("0", JInt(100), "1", JInt(300), "2", JBool(true), "3", JFalse(), "4", JFloat(1), "5", JString("111"));
	obj.removeByType(TYPE_FALSE);
	approve(obj.getItemsCount() == 5);
	obj.removeByType(TYPE_STRING);
	approve(obj.getItemsCount() == 4);
	obj.removeByType(TYPE_STRING);
	approve(obj.getItemsCount() == 4);
	obj.removeByType(TYPE_INT);
	approve(obj.getItemsCount() == 2);
	obj.removeByType(TYPE_FLOAT);
	approve(obj.getItemsCount() == 1);
	approve(obj["2"].isTrue());
};

NTEST(test_method_array_removeByValue) {
	auto arr = JArray(JInt(100), JInt(300), JBool(true), JFalse(), JFloat(1), JString("111"));
	arr.removeByValue(JTrue());
	approve(arr.getItemsCount() == 5);
	arr.removeByValue(JString(""));
	approve(arr.getItemsCount() == 5);
	arr.removeByValue(JString("111"));
	approve(arr.getItemsCount() == 4);
	arr.removeByValue(JInt(300));
	approve(arr.getItemsCount() == 3);
};

NTEST(test_method_object_removeByValue) {
	auto obj = JObject("0", JInt(100), "1", JInt(300), "2", JBool(true), "3", JFalse(), "4", JFloat(1), "5", JString("111"));
	obj.removeByValue(JTrue());
	approve(obj.getItemsCount() == 5);
	obj.removeByValue(JString(""));
	approve(obj.getItemsCount() == 5);
	obj.removeByValue(JString("111"));
	approve(obj.getItemsCount() == 4);
	obj.removeByValue(JInt(300));
	approve(obj.getItemsCount() == 3);
};

NTEST(test_method_append) {
	auto arr = JArray(JInt(100), JInt(300));
	auto arr2 = JArray(JArray(), JTrue());
	arr.append(arr2);
	approve(arr2.getItemsCount() == 2);
	approve(arr.getItemsCount() == 4);
	approve(arr.atIndex(2) && arr[2].isArray());
	approve(arr.atIndex(3) && arr[3].isTrue());

	arr.append( JArray(JString("qwe"), JFloat(3)) );
	approve(arr.getItemsCount() == 6);
	approve(arr.atIndex(4) && arr[4].isString());
	approve(arr.atIndex(5) && arr[5].isFloat());
};

NTEST(test_method_merge) {
	auto obj = JObject("0", JInt(100), "1", JInt(300));
	auto obj2 = JObject("2", JArray(), "3", JTrue());
	obj.merge(obj2);
	approve(obj2.getItemsCount() == 2);
	approve(obj.getItemsCount() == 4);
	approve(obj.atKey("2") && obj["2"].isArray());
	approve(obj.atKey("3") && obj["3"].isTrue());

	obj.merge( JObject("4", JString("qwe"), "5", JFloat(3)) );
	approve(obj.getItemsCount() == 6);
	approve(obj.atKey("4") && obj["4"].isString());
	approve(obj.atKey("5") && obj["5"].isFloat());

	obj.merge( JObject("2", JObject(), "3", JInt(100) ) );
	approve(obj.getItemsCount() == 6);
	approve(obj.atKey("2") && obj["2"].isObject());
	approve(obj.atKey("3") && obj["3"].isInt());
};
