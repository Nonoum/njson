/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/Record.h"

using namespace njson;

NTEST(test_copy_int) {
	Record r(TYPE_INT);
	r.setInt(IntType(0x12345678));
	Record r2 = r, r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = r;
	approve(r2.getInt() == IntType(0x12345678));
	approve(r3.getInt() == IntType(0x12345678));
	{
		Record tmp(TYPE_INT);
		tmp.setInt(IntType(0x98765432));
		r4 = tmp;
		tmp.setInt(150);
	}
	approve(r4.getInt() == IntType(0x98765432));
};

NTEST(test_move_int) {
	Record r(TYPE_INT);
	r.setInt(IntType(0x12345678));
	Record r2(std::move(r));
	approve(r2.getInt() == IntType(0x12345678));
	Record r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = std::move(r2);
	approve(r3.getInt() == IntType(0x12345678));
	{
		Record tmp(TYPE_INT);
		tmp.setInt(IntType(0x98765432));
		r4 = std::move(tmp);
	}
	approve(r4.getInt() == IntType(0x98765432));
};

NTEST(test_copy_string) {
	Record r(TYPE_STRING);
	r.setString("String to copy");
	Record r2 = r, r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = r;
	approve(r2.getString() == "String to copy");
	approve(r3.getString() == "String to copy");
	{
		Record tmp(TYPE_STRING);
		tmp.setString("STRING TO COPY");
		r4 = tmp;
		tmp.setString("trash");
	}
	approve(r4.getString() == "STRING TO COPY");
};

NTEST(test_move_string) {
	Record r(TYPE_STRING);
	r.setString("String To Move");
	Record r2(std::move(r));
	approve(r2.getString() == "String To Move");
	Record r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = std::move(r2);
	approve(r3.getString() == "String To Move");
	{
		Record tmp(TYPE_STRING);
		tmp.setString("STRING TO MOVE");
		r4 = std::move(tmp);
	}
	approve(r4.getString() == "STRING TO MOVE");
};

NTEST(test_copy_wstring) {
	Record r(TYPE_WSTRING);
	r.setWString(L"String to copy");
	Record r2 = r, r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = r;
	approve(r2.getWString() == L"String to copy");
	approve(r3.getWString() == L"String to copy");
	{
		Record tmp(TYPE_WSTRING);
		tmp.setWString(L"STRING TO COPY");
		r4 = tmp;
		tmp.setWString(L"trash");
	}
	approve(r4.getWString() == L"STRING TO COPY");
};

NTEST(test_move_wstring) {
	Record r(TYPE_WSTRING);
	r.setWString(L"String To Move");
	Record r2(std::move(r));
	approve(r2.getWString() == L"String To Move");
	Record r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = std::move(r2);
	approve(r3.getWString() == L"String To Move");
	{
		Record tmp(TYPE_WSTRING);
		tmp.setWString(L"STRING TO MOVE");
		r4 = std::move(tmp);
	}
	approve(r4.getWString() == L"STRING TO MOVE");
};

NTEST(test_copy_array) {
	Record r(TYPE_ARRAY);
	{
		Record r1(TYPE_INT), r2(TYPE_STRING);
		r1.setInt(IntType(0x88442211));
		r2.setString("qazwsxedc");
		r.pushBack(r1);
		r.pushBack(r2);
	}
	Record r2 = r, r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = r;
	r[0].setInt(123);
	r[1].setString("asd");
	approve(r2[0].getInt() == IntType(0x88442211));
	approve(r2[1].getString() == "qazwsxedc");
	approve(r3[0].getInt() == IntType(0x88442211));
	approve(r3[1].getString() == "qazwsxedc");
	{
		Record tmp(TYPE_ARRAY);
		Record tmp2(TYPE_STRING);
		tmp2.setString("11119999");
		tmp.pushBack(tmp2);
		r4 = tmp;
		tmp2.setString("asd");
	}
	approve(r4[0].getString() == "11119999");
};

NTEST(test_move_array) {
	Record r(TYPE_ARRAY);
	{
		Record r1(TYPE_INT), r2(TYPE_STRING);
		r1.setInt(IntType(0x88442211));
		r2.setString("qazwsxedc");
		r.pushBack(r1);
		r.pushBack(r2);
	}
	Record r2(std::move(r)), r3(TYPE_NULL), r4(TYPE_NULL);
	approve(r2[0].getInt() == IntType(0x88442211));
	approve(r2[1].getString() == "qazwsxedc");
	r3 = std::move(r2);
	approve(r3[0].getInt() == IntType(0x88442211));
	approve(r3[1].getString() == "qazwsxedc");
	{
		Record tmp(TYPE_ARRAY);
		Record tmp2(TYPE_STRING);
		tmp2.setString("11119999");
		tmp.pushBack(tmp2);
		r4 = std::move(tmp);
	}
	approve(r4[0].getString() == "11119999");
};

NTEST(test_copy_object) {
	Record r(TYPE_OBJECT);
	{
		Record r1(TYPE_INT), r2(TYPE_STRING);
		r1.setInt(IntType(0x88442211));
		r2.setString("qazwsxedc");
		r["first"] = r1;
		r["second"] = r2;
	}
	Record r2 = r, r3(TYPE_NULL), r4(TYPE_NULL);
	r3 = r;
	r["first"].setInt(123);
	r["second"].setString("asd");
	approve(r2["first"].getInt() == IntType(0x88442211));
	approve(r2["second"].getString() == "qazwsxedc");
	approve(r3["first"].getInt() == IntType(0x88442211));
	approve(r3["second"].getString() == "qazwsxedc");
	{
		Record tmp(TYPE_OBJECT);
		Record tmp2(TYPE_STRING);
		tmp2.setString("11119999");
		tmp["third"] = tmp2;
		r4 = tmp;
		tmp2.setString("asd");
	}
	approve(r4["third"].getString() == "11119999");
};

NTEST(test_move_object) {
	Record r(TYPE_OBJECT);
	{
		Record r1(TYPE_INT), r2(TYPE_STRING);
		r1.setInt(IntType(0x88442211));
		r2.setString("qazwsxedc");
		r["first"] = r1;
		r["second"] = r2;
	}
	Record r2(std::move(r)), r3(TYPE_NULL), r4(TYPE_NULL);
	approve(r2["first"].getInt() == IntType(0x88442211));
	approve(r2["second"].getString() == "qazwsxedc");
	r3 = std::move(r2);
	approve(r3["first"].getInt() == IntType(0x88442211));
	approve(r3["second"].getString() == "qazwsxedc");
	{
		Record tmp(TYPE_OBJECT);
		Record tmp2(TYPE_STRING);
		tmp2.setString("11119999");
		tmp["third"] = tmp2;
		r4 = std::move(tmp);
	}
	approve(r4["third"].getString() == "11119999");
};
