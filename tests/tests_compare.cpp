/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../ntest/ntest.h"
#include "../headers/Record.h"

using namespace njson;

inline bool CompareEqual(const Record& left, const Record& right) {
	return (left == right) && (! (left != right));
};

static const auto all_types_list = {TYPE_STRING, TYPE_WSTRING, TYPE_ARRAY, TYPE_OBJECT,
									TYPE_INT, TYPE_FLOAT,
									TYPE_TRUE, TYPE_FALSE, TYPE_NULL};

NTEST(test_compare_generic_empty_equal) {
	for(auto type : all_types_list) {
		Record r1(type), r2(type);
		approve(CompareEqual(r1, r2));
	}
};

NTEST(test_compare_generic_empty_not_equal) {
	for(auto t1 : all_types_list) {
		for(auto t2 : all_types_list) {
			if(t1 != t2) {
				Record r1(t1), r2(t2);
				approve(! CompareEqual(r1, r2));
			}
		}
	}
};

NTEST(test_compare_bool) {
	Record r1(TYPE_TRUE), r2(TYPE_FALSE), r3(TYPE_TRUE), r4(TYPE_FALSE);
	approve(CompareEqual(r1, r3));
	approve(CompareEqual(r2, r4));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r1));
	r1.setBool(false);
	approve(CompareEqual(r1, r2));
	r4.setBool(true);
	approve(CompareEqual(r3, r4));
};

NTEST(test_compare_int) {
	Record r1(TYPE_INT), r2(TYPE_INT), r3(TYPE_INT);
	r1.setInt(50);
	r2.setInt(100);
	r3.setInt(50);
	approve(CompareEqual(r1, r3));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r3));
	r3.setInt(100);
	approve(! CompareEqual(r1, r3));
	approve(CompareEqual(r2, r3));
};

NTEST(test_compare_string) {
	Record r1(TYPE_STRING), r2(TYPE_STRING), r3(TYPE_STRING);
	r1.setString("asdfg");
	r2.setString("zxc");
	r3.setString("asdfg");
	approve(CompareEqual(r1, r3));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r3));
	r3.setString("zxc");
	approve(! CompareEqual(r1, r3));
	approve(CompareEqual(r2, r3));
};

NTEST(test_compare_wstring) {
	Record r1(TYPE_WSTRING), r2(TYPE_WSTRING), r3(TYPE_WSTRING);
	r1.setWString(L"asdfg");
	r1.setWString(r1.getWString() + WStringType(5, 0x1E00));
	r2.setWString(L"zxc");
	r3.setWString(L"asdfg");
	r3.setWString(r3.getWString() + WStringType(5, 0x1E00));
	approve(CompareEqual(r1, r3));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r3));
	r3.setWString(L"zxc");
	approve(! CompareEqual(r1, r3));
	approve(CompareEqual(r2, r3));
};

NTEST(test_compare_array) {
	Record r1(TYPE_ARRAY), r2(TYPE_ARRAY), r3(TYPE_ARRAY);
	Record rg1(TYPE_INT), rg2(TYPE_INT);
	rg1.setInt(1234);
	rg2.setInt(5678);
	r1.pushBack(rg1);
	r2.pushBack(rg1);
	r3.pushBack(rg1);
	r2.pushBack(rg2);
	approve(CompareEqual(r1, r3));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r3));
	r3.pushBack(rg2);
	approve(! CompareEqual(r1, r3));
	approve(CompareEqual(r2, r3));
};

NTEST(test_compare_object) {
	Record r1(TYPE_OBJECT), r2(TYPE_OBJECT), r3(TYPE_OBJECT);
	Record rg1(TYPE_INT), rg2(TYPE_INT);
	rg1.setInt(1234);
	rg2.setInt(5678);
	r1["first"] = rg1;
	r2["first"] = rg1;
	r3["first"] = rg1;
	r2["second"] = rg2;
	approve(CompareEqual(r1, r3));
	approve(! CompareEqual(r1, r2));
	approve(! CompareEqual(r2, r3));
	r3["second"] = rg2;
	approve(! CompareEqual(r1, r3));
	approve(CompareEqual(r2, r3));
};
