/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../headers/Record.h"
#include "data_readers.h"
#include "data_writers.h"
#include "string_builders.h"
#include "format_helpers.h"

#include <algorithm>
#include <stdio.h>
#include <assert.h>

#define NJSON_CASE_WHITESPACES_BREAK \
	case ' ': \
	case '\n': \
	case '\r': \
	case '\t': \
		break

#ifdef _MSC_VER
template <typename... Args> int snprintf(char* buf, size_t length, const char* format, Args... args) {
	return sprintf_s(buf, length, format, args...);
};
#endif

namespace njson {

static const size_t string_with_number_length_limit = 100;

// ObjectMap

class Record::ObjectMap {
	NIndexedVector<Pair> pairs;

	static inline bool ComparePairToKey(const Pair& pair, const char* key) {
		return pair.first < key;
	};
	static inline bool ComparePairToStringKey(const Pair& pair, const std::string& key) {
		return pair.first < key;
	};
public:
	ObjectMap() {};
	ObjectMap(const ObjectMap& other) : pairs(other.pairs) {};
	ObjectMap(ObjectMap&& other) : pairs(std::move(other.pairs)) {};
	void operator =(const ObjectMap& other) { pairs = other.pairs; };
	void operator =(ObjectMap&& other) { pairs = std::move(other.pairs); };
	bool operator ==(const ObjectMap& other) const { return pairs == other.pairs; };

	size_t size() const { return pairs.size(); };
	void reserve(size_t count) { pairs.reserve(count); };

	void emplace(const char* key, Record&& value);
	void emplace(Pair&& pair);

	Record* find(const char* key);
	const Record* find(const char* key) const;

	bool remove(const char* key);
	void iterate(const std::function<void(const std::string& key, Record& value)>& func);
	void iterate(const std::function<void(const std::string& key, const Record& value)>& func) const;

	void removeIf(const std::function<bool(const std::string& key, const Record& value)>& func);
};

void Record::ObjectMap :: emplace(const char* key, Record&& value) {
	auto found = std::lower_bound(pairs.begin(), pairs.end(), key, ComparePairToKey);
	if((found == pairs.end()) || (found->first != key)) {
		pairs.emplace(found, std::string(key), std::forward<Record>(value));
	} else {
		found->second = std::move(value);
	}
};

void Record::ObjectMap :: emplace(Pair&& pair) {
	auto found = std::lower_bound(pairs.begin(), pairs.end(), pair.first, ComparePairToStringKey);
	if((found == pairs.end()) || (found->first != pair.first)) {
		pairs.emplace(found, std::forward<Pair>(pair));
	} else {
		found->second = std::move(pair.second);
	}
};

Record* Record::ObjectMap :: find(const char* key) {
	auto found = std::lower_bound(pairs.begin(), pairs.end(), key, ComparePairToKey);
	return (found != pairs.end()) && found->first == key ? &found->second : NULL;
};

const Record* Record::ObjectMap :: find(const char* key) const {
	auto found = std::lower_bound(pairs.begin(), pairs.end(), key, ComparePairToKey);
	return (found != pairs.end()) && found->first == key ? &found->second : NULL;
};

bool Record::ObjectMap :: remove(const char* key) {
	auto found = std::lower_bound(pairs.begin(), pairs.end(), key, ComparePairToKey);
	if((found != pairs.end()) && (found->first == key)) {
		pairs.erase(found);
		return true;
	}
	return false;
};

inline void Record::ObjectMap :: iterate(const std::function<void(const std::string& key, Record& value)>& func) {
	std::for_each(pairs.begin(), pairs.end(), [&func](Pair& p) { func(p.first, p.second); });
};

inline void Record::ObjectMap :: iterate(const std::function<void(const std::string& key, const Record& value)>& func) const {
	std::for_each(pairs.begin(), pairs.end(), [&func](const Pair& p) { func(p.first, p.second); });
};

inline void Record::ObjectMap :: removeIf(const std::function<bool(const std::string& key, const Record& value)>& func) {
	pairs.erase(std::remove_if(pairs.begin(), pairs.end(), [&func](const Pair& p) { return func(p.first, p.second); }),
				true);
};

// Record

static Record& AccessStaticErrorRecord() {
	static Record rec(TYPE_NULL);
	return rec;
};

Record :: Record(DataType _type) {
	init(_type);
};

Record :: Record(const Record& other) {
	type = other.type;
	switch (type) {
	case TYPE_STRING: new (&u.data) StringType(*other.accessString()); break;
	case TYPE_WSTRING: new (&u.data) WStringType(*other.accessWString()); break;
	case TYPE_ARRAY: new (&u.data) ArrayType(*other.accessArray()); break;
	case TYPE_OBJECT: new (&u.data) ObjectMap(*other.accessObjectMap()); break;
	case TYPE_INT: u.int_value = other.u.int_value; break;
	case TYPE_FLOAT: u.float_value = other.u.float_value; break;
	default: break;
	};
};

Record :: Record(Record&& other) {
	type = other.type;
	switch (type) {
	case TYPE_STRING: new (&u.data) StringType(std::move(*other.accessString())); break;
	case TYPE_WSTRING: new (&u.data) WStringType(std::move(*other.accessWString())); break;
	case TYPE_ARRAY: new (&u.data) ArrayType(std::move(*other.accessArray())); break;
	case TYPE_OBJECT: new (&u.data) ObjectMap(std::move(*other.accessObjectMap())); break;
	case TYPE_INT: u.int_value = other.u.int_value; break;
	case TYPE_FLOAT: u.float_value = other.u.float_value; break;
	default: break;
	};
};

Record :: ~Record() {
	clear();
};

Record& Record :: operator=(const Record& other) {
	if(type != other.type) {
		clear();
		new (this) Record(other);
		return *this;
	}
	switch (type) {
	case TYPE_STRING: *accessString() = *other.accessString(); break;
	case TYPE_WSTRING: *accessWString() = *other.accessWString(); break;
	case TYPE_ARRAY: *accessArray() = *other.accessArray(); break;
	case TYPE_OBJECT: *accessObjectMap() = *other.accessObjectMap(); break;
	case TYPE_INT: u.int_value = other.u.int_value; break;
	case TYPE_FLOAT: u.float_value = other.u.float_value; break;
	default: break;
	};
	return *this;
};

Record& Record :: operator=(Record&& other) {
	clear();
	new (this) Record(std::forward<Record>(other));
	return *this;
};

void Record :: clear() {
	switch (type) {
	case TYPE_STRING: accessString()->~StringType(); break;
	case TYPE_WSTRING: accessWString()->~WStringType(); break;
	case TYPE_ARRAY: accessArray()->~ArrayType(); break;
	case TYPE_OBJECT: accessObjectMap()->~ObjectMap(); break;
	default: break;
	};
	type = TYPE_NULL;
#ifdef NJSON_DEBUG
	for(size_t i = 0; i < sizeof(u); ++i) {
	   (&u.data)[i] = 0;
	}
#endif
};

void Record :: init(DataType _type) {
	type = _type;
	switch (type) {
	case TYPE_STRING: new (&u.data) StringType(); break;
	case TYPE_WSTRING: new (&u.data) WStringType(); break;
	case TYPE_ARRAY: new (&u.data) ArrayType(); break;
	case TYPE_OBJECT: new (&u.data) ObjectMap(); break;
	case TYPE_INT: u.int_value = 0; break;
	case TYPE_FLOAT: u.float_value = 0; break;
	default: break;
	};
};

// comparsions ------------------------------------

bool Record :: operator ==(const Record& other) const {
	if(type != other.type) {
		return false;
	}
	switch (type) {
	case TYPE_STRING: return *accessString() == *other.accessString();
	case TYPE_WSTRING: return *accessWString() == *other.accessWString();
	case TYPE_ARRAY: return *accessArray() == *other.accessArray();
	case TYPE_OBJECT: return *accessObjectMap() == *other.accessObjectMap();
	case TYPE_INT: return u.int_value == other.u.int_value;
	case TYPE_FLOAT: return u.float_value == other.u.float_value;
	default: break;
	};
	return true;
};

bool Record :: operator !=(const Record& other) const {
	return ! operator==(other);
};

// getters / setters ------------------------------------

bool Record :: getBool() const {
	assert(isBool());
	return isTrue();
};

void Record :: setBool(bool val) {
	assert(isBool());
	if(isBool()) {
		type = (val ? TYPE_TRUE : TYPE_FALSE);
	}
};

IntType Record :: getInt() const {
	assert(isInt());
	return u.int_value;
};

void Record :: setInt(IntType val) {
	assert(isInt());
	if(isInt()) {
		u.int_value = val;
	}
};

FloatType Record :: getNumber() const {
	assert(isNumber());
	if(isInt()) {
		return u.int_value;
	} else if(isFloat()) {
		return u.float_value;
	}
	return 0;
};

void Record :: setNumber(FloatType val) {
	assert(isNumber());
	if(isInt()) {
		u.int_value = IntType(val);
	} else if(isFloat()) {
		u.float_value = val;
	}
};

FloatType Record :: getFloat() const {
	assert(isFloat());
	return u.float_value;
};

void Record :: setFloat(FloatType val) {
	assert(isFloat());
	if(isFloat()) {
		u.float_value = val;
	}
};

// string

const StringType& Record :: getString() const {
	static const StringType static_error_str;
	assert(isString());
	return isString() ? *accessString() : static_error_str;
};

// UNICODE string

const WStringType& Record :: getWString() const {
	static const WStringType static_error_str;
	assert(isWString());
	return isWString() ? *accessWString() : static_error_str;
};

// common API of collections

size_t Record :: getItemsCount() const {
	assert(isCollection());
	if(isArray()) {
		return accessArray()->size();
	} else if(isObject()) {
		return accessObjectMap()->size();
	}
	return 0;
};

void Record :: reserve(size_t count) {
	assert(isCollection());
	if(isArray()) {
		accessArray()->reserve(count);
	} else if(isObject()) {
		accessObjectMap()->reserve(count);
	}
};

void Record :: removeByType(DataType t) {
	assert(isCollection());
	if(isArray()) {
		accessArray()->erase(std::remove_if(accessArray()->begin(), accessArray()->end(),
											[t](const Record& r) { return r.type == t; }),
							accessArray()->end());
	} else if(isObject()) {
		accessObjectMap()->removeIf([t](const std::string& key, const Record& value) { return value.type == t; });
	}
};

void Record :: removeByValue(Record record) {
	assert(isCollection());
	if(isArray()) {
		accessArray()->erase(std::remove_if(accessArray()->begin(), accessArray()->end(),
											[&record](const Record& r) { return r == record; }),
							accessArray()->end());
	} else if(isObject()) {
		accessObjectMap()->removeIf([&record](const std::string& key, const Record& value) { return value == record; });
	}
};

// TYPE_ARRAY's API

Record& Record :: operator [](size_t index) {
	assert(isArray() && (index < accessArray()->size()));
	if(isArray() && (index < accessArray()->size())) {
		return (*accessArray())[index];
	}
	return AccessStaticErrorRecord();
};

Record* Record :: atIndex(size_t index) {
	assert(isArray());
	if(isArray() && (index < accessArray()->size())) {
		return &(*accessArray())[index];
	}
	return NULL;
};

const Record* Record :: atIndex(size_t index) const {
	assert(isArray());
	if(isArray() && (index < accessArray()->size())) {
		return &(*accessArray())[index];
	}
	return NULL;
};

size_t Record :: indexOf(const Record& record) {
	assert(isArray());
	if(isArray()) {
		auto arr = accessArray();
		auto first = &*(arr->begin());
		auto last = first + arr->size();
		if( ((&record) >= first) && ((&record) < last) ) { // hardcore :p
			return (&record) - first;
		}
		auto found = std::find(arr->begin(), arr->end(), record);
		return found == arr->end() ? size_t(-1) : found - arr->begin();
	}
	return size_t(-1);
};

void Record :: insert(size_t index, const Record& record) {
	assert(isArray());
	if(isArray()) {
		auto arr = accessArray();
		auto pos = (index < arr->size()) ? arr->begin() + index : arr->end();
		arr->insert(pos, record);
	}
};

void Record :: insert(size_t index, Record&& record) {
	assert(isArray());
	if(isArray()) {
		auto arr = accessArray();
		auto pos = (index < arr->size()) ? arr->begin() + index : arr->end();
		arr->insert(pos, std::forward<Record>(record));
	}
};

void Record :: pushBack(const Record& record) {
	assert(isArray());
	if(isArray()) {
		accessArray()->push_back(record);
	}
};

void Record :: pushBack(Record&& record) {
	assert(isArray());
	if(isArray()) {
		accessArray()->push_back(std::forward<Record>(record));
	}
};

bool Record :: remove(size_t index) {
	assert(isArray() && (index < accessArray()->size()));
	if(isArray() && (index < accessArray()->size())) {
		auto arr = accessArray();
		arr->erase(arr->begin() + index);
	}
	return false;
};

void Record :: append(const Record& other_array) {
	assert(isArray() && other_array.isArray());
	if(isArray() && other_array.isArray()) {
		for(auto& r : *other_array.accessArray()) {
			accessArray()->push_back(r);
		}
	}
};

void Record :: append(Record&& other_array) {
	assert(isArray() && other_array.isArray());
	if(isArray() && other_array.isArray()) {
		for(auto& r : *other_array.accessArray()) {
			accessArray()->push_back( std::move(r) );
		}
		other_array.clear();
	}
};

// TYPE_OBJECT's API

Record& Record :: operator [](const KeyType key) {
	return get(key, Record(TYPE_NULL));
};

Record& Record :: get(const KeyType key, Record&& default_value) {
	assert(isObject() && key && key[0]);
	if(isObject() && key && key[0]) {
		auto rec_ptr = accessObjectMap()->find(key);
		if(! rec_ptr) {
			accessObjectMap()->emplace(key, std::move(default_value));
			return *accessObjectMap()->find(key);
		}
		return *rec_ptr;
	}
	return AccessStaticErrorRecord();
};

Record* Record :: atKey(const char* key) {
	assert(isObject() && key);
	return isObject() && key ? accessObjectMap()->find(key) : NULL;
};

const Record* Record :: atKey(const char* key) const {
	assert(isObject() && key);
	return isObject() && key ? accessObjectMap()->find(key) : NULL;
};

void Record :: assign(const char* key, Record&& value) {
	assert(isObject() && key && key[0]);
	if(isObject() && key && key[0]) {
		accessObjectMap()->emplace(key, std::forward<Record>(value));
	}
};

bool Record :: removeKey(const char* key) {
	assert(isObject());
	return isObject() && key ? accessObjectMap()->remove(key) : false;
};

void Record :: merge(const Record& other_object) {
	assert(isObject() && other_object.isObject());
	if(isObject() && other_object.isObject()) {
		other_object.accessObjectMap()->iterate([this](const std::string& key, const Record& value) {
			operator[](key.c_str()) = value;
		});
	}
};

void Record :: merge(Record&& other_object) {
	assert(isObject() && other_object.isObject());
	if(isObject() && other_object.isObject()) {
		other_object.accessObjectMap()->iterate([this](const std::string& key, Record& value) {
			accessObjectMap()->emplace(key.c_str(), std::move(value));
		});
		other_object.clear();
	}
};

void Record :: iterate(const std::function<void(const std::string& key, Record& value)>& func) {
	assert(isObject());
	if(isObject()) {
		accessObjectMap()->iterate(func);
	}
};

void Record :: iterate(const std::function<void(const std::string& key, const Record& value)>& func) const {
	assert(isObject());
	if(isObject()) {
		accessObjectMap()->iterate(func);
	}
};

// private readers

template <class CharType, class SourceType>
Record Record :: ParseAnything(SourceType& src, ParsingState* state) {
	CharType ch;
	while(src.get(ch)) {
		switch (ch) {
		case '[': return ParseArray<CharType>(src, state);
		case '{': return ParseObject<CharType>(src, state);
		case '\"': return ParseString<CharType, AnyStringBuilder>(src, state);
		NJSON_CASE_WHITESPACES_BREAK;
		default:
			src.recover();
			return ParseConstant<CharType>(src, state);
		};
	}
	state->setNoData(src.pos(), TYPE_NULL);
	return Record(TYPE_NULL);
};

template <class CharType, class SourceType>
Record Record :: ParseConstant(SourceType& src, ParsingState* state) {
	Record result(TYPE_NULL);
	CharType ch;
	auto complete = [&ch, &src, &result, state](const char* match_sequence, DataType t) {
		while((*match_sequence != 0) && (src.get(ch))) {
			if(char(ch) != *match_sequence) {
				state->setBadChar(src.pos(), ch, t);
				return;
			}
			++match_sequence;
		}
		if(*match_sequence == 0) {
			result.init(t);
		} else {
			state->setNoData(src.pos(), t);
		}
	};
	if(src.get(ch)) {
		switch (ch) {
		case 't': complete("rue", TYPE_TRUE); break;
		case 'f': complete("alse", TYPE_FALSE); break;
		case 'n': complete("ull", TYPE_NULL); break;
		default: { // number
				src.recover();
				char buf[string_with_number_length_limit];
				size_t len = 0;
				int n_dots = 0, n_exps = 0, n_digits = 0;
				while(src.get(ch)) {
					if(isdigit(ch)) {
						++n_digits;
					} else if((ch == '+') || (ch == '-')) {
						if((len != 0) && (buf[len-1] != 'e') && (buf[len-1] != 'E')) {
							src.recover(); // this character isn't belong to this value
							break;
						}
					} else if((ch == 'e') || (ch == 'E')) {
						if(n_exps - n_dots > 0) {
							src.recover(); // this character isn't belong to this value
							break;
						}
						++n_exps;
					} else if(ch == '.') {
						if(n_dots) {
							src.recover(); // this character isn't belong to this value
							break;
						}
						++n_dots;
					} else {
						src.recover(); // this character isn't belong to this value
						break;
					}
					if(len == sizeof(buf) - 1) {
						src.recover(); // this character isn't belong to this value
						break;
					}
					buf[len] = char(ch);
					++len;
				}
				if(! n_digits) {
					state->setBadNumber(src.pos());
					return Record(TYPE_NULL);
				}
				buf[len] = 0;
				if(n_dots || n_exps) {
					double num;
					if(sscanf(buf, "%lf", &num)) {
						result.init(TYPE_FLOAT);
						result.u.float_value = num;
					}
				} else {
					long num;
					if(sscanf(buf, "%ld", &num)) {
						result.init(TYPE_INT);
						result.u.int_value = num;
					}
				}
				if(result.isNull()) { // parsing error
					state->setBadNumber(src.pos());
					return Record(TYPE_NULL);
				}
				break;
			}
		};
	} else {
		state->setNoData(src.pos(), TYPE_NULL);
	}
	return result;
};

template <class CharType, class SourceType>
Record Record :: ParseArray(SourceType& src, ParsingState* state) {
	Record result(TYPE_ARRAY);
	CharType ch;
	bool expect_value = true;
	while(src.get(ch)) {
		switch (ch) {
		NJSON_CASE_WHITESPACES_BREAK;
		case ']':
			if(result.accessArray()->size() && expect_value) {
				state->setNoData(src.pos(), TYPE_ARRAY);
			}
			return result;
		case ',':
			if(expect_value) {
				state->setBadChar(src.pos(), ch, TYPE_ARRAY);
				return result;
			}
			expect_value = true;
			break;
		default:
			if(! expect_value) {
				state->setBadChar(src.pos(), ch, TYPE_ARRAY);
				return result;
			}
			src.recover();
			result.accessArray()->push_back( ParseAnything<CharType>(src, state) );
			if(state->result != ParsingError::SUCCESS) {
				return result;
			}
			expect_value = false;
			break;
		};
	}
	state->setNoData(src.pos(), TYPE_ARRAY);
	return result;
};

template <class CharType, class SourceType>
Record Record :: ParseObject(SourceType& src, ParsingState* state) {
	Record result(TYPE_OBJECT);
	CharType ch;
	bool expect_value = true;
	while(src.get(ch)) {
		switch (ch) {
		NJSON_CASE_WHITESPACES_BREAK;
		case '}':
			if(result.accessObjectMap()->size() && expect_value) {
				state->setNoData(src.pos(), TYPE_OBJECT);
			}
			return result;
		case ',':
			if(expect_value) {
				state->setBadChar(src.pos(), ch, TYPE_OBJECT);
				return result;
			}
			expect_value = true;
			break;
		default: {
				if(! expect_value) {
					state->setBadChar(src.pos(), ch, TYPE_OBJECT);
					return result;
				}
				src.recover();
				auto pair = ParseKeyValue<CharType>(src, state);
				if(pair.first.length()) { // add if key has non-zero length, even if there is parsing error
					result.accessObjectMap()->emplace(std::move(pair));
				}
				if(state->result != ParsingError::SUCCESS) {
					return result;
				}
				expect_value = false;
				break;
			}
		};
	}
	state->setNoData(src.pos(), TYPE_OBJECT);
	return result;
};

template <class CharType, class StringBuilderType, class SourceType>
Record Record :: ParseString(SourceType& src, ParsingState* state) {
	StringBuilderType builder;
	CharType ch;
	bool escaped = false, succeeded = false;
	while(src.get(ch)) {
		if(ch == '\\') {
			escaped ^= true;
			if(! escaped) {
				builder.onRegularChar(ch);
			}
			continue;
		} else if((ch == '\"') && (! escaped)) {
			succeeded = true;
			break; // string completed by unescaped quote
		}
		if(escaped) {
			CharType c_out;
			switch (ch) {
			case '/': c_out = '/'; break;
			case 'b': c_out = '\b'; break;
			case 'f': c_out = '\f'; break;
			case 'n': c_out = '\n'; break;
			case 'r': c_out = '\r'; break;
			case 't': c_out = '\t'; break;
			case 'u':
				c_out = 'u'; // only for suppressing gcc's warning
				if(! builder.template onBackSlashU<CharType>(src, state)) {
					return builder.obtainResult();
				}
				break;
			default:
				c_out = ch;
				break;
			};
			if(ch != 'u') {
				builder.onRegularChar(c_out);
			}
			escaped = false;
		} else {
			builder.onRegularChar(ch);
		}
	}
	if(! succeeded) {
		state->setNoData(src.pos(), builder.lastType());
	}
	return builder.obtainResult();
};

template <class CharType, class SourceType>
Record::Pair Record :: ParseKeyValue(SourceType& src, ParsingState* state) {
	CharType ch;
	std::string tmp_key;
	if(src.get(ch) && (ch == '\"')) {
		auto rec = ParseString<CharType, AsciiStringBuilder>(src, state);
		if(rec.isString()) {
			tmp_key = std::move(*rec.accessString());
		}
	}
	if(! tmp_key.length()) {
		state->setEmptyKey(src.pos());
	}
	Pair result(std::move(tmp_key), Record(TYPE_NULL));
	if(state->result != ParsingError::SUCCESS) {
		return result;
	}

	while(src.get(ch)) {
		switch (ch) {
		case ':':
			result.second = ParseAnything<CharType>(src, state);
			return result;
		NJSON_CASE_WHITESPACES_BREAK;
		default:
			state->setBadChar(src.pos(), ch, TYPE_OBJECT);
			break;
		};
	}
	state->setNoData(src.pos(), TYPE_OBJECT);
	return result;
};

// writers

size_t Record :: estimate(FormatCalculations* fmt) const {
	assert(fmt != NULL);
	switch (type) {
	case TYPE_STRING:
		return accessString()->length() + 2 + fmt->pre_indent_total_len;
	case TYPE_WSTRING:
		if(fmt->output_char_size == 1) {
			return accessWString()->length() * 6 + 2 + fmt->pre_indent_total_len; // multiply on 6 because of transforming chars to \uXXXX form
		}
		return accessWString()->length() + 2 + fmt->pre_indent_total_len;
	case TYPE_ARRAY: {
			size_t result = 2 + fmt->pre_indent_total_len + fmt->after_block_open_len;
			fmt->pre_indent_total_len += fmt->pre_indent_len;
			for(auto& r : *accessArray()) {
				result += r.estimate(fmt);
			}
			result += accessArray()->size() * (1 + fmt->after_value_len);
			fmt->pre_indent_total_len -= fmt->pre_indent_len;
			return result;
		}
		break;
	case TYPE_OBJECT: {
			size_t result = 2 + fmt->pre_indent_total_len + fmt->after_block_open_len;
			fmt->pre_indent_total_len += fmt->pre_indent_len;
			auto obj_map = accessObjectMap();
			obj_map->iterate([&result, fmt](const std::string& key, const Record& value) {
				result += 2 + key.length() + 1 + value.estimate(fmt);
			});
			result += accessObjectMap()->size() * (1 + fmt->after_value_len + (fmt->around_colon_len * 2));
			fmt->pre_indent_total_len -= fmt->pre_indent_len;
			return result;
		}
		break;
	case TYPE_INT:
		return 6 + fmt->pre_indent_total_len;
	case TYPE_FLOAT:
		return 11 + fmt->pre_indent_total_len;
	case TYPE_FALSE:
	case TYPE_TRUE:
	case TYPE_NULL:
		return 5 + fmt->pre_indent_total_len;
	};
	return 0;
};

template <class WriterType>
static inline void WritePreIndent(WriterType& dst, FormatCalculations* fmt) {
	if(fmt->pre_indent_total_len) {
		for(size_t i = 0; i < fmt->pre_indent_total_cnt; ++i) {
			dst.append(fmt->info.pre_indent);
		}
	}
};

template <class WriterType>
static inline void WriteAfterValue(WriterType& dst, FormatCalculations* fmt) {
	if(fmt->after_value_len) {
		dst.append(fmt->info.after_value);
	}
};

template <class SrcType, class WriterType>
static void WriteString(const SrcType& src, WriterType& dst) {
	dst.pushTransformedChar('\"');
	auto data = src.c_str();
	for(size_t i = 0; i < src.length(); ++i) {
		auto ch = data[i];
		switch (ch) {
		case '/': dst.pushTransformedChar('\\'); dst.pushTransformedChar('/'); break;
		case '"': dst.pushTransformedChar('\\'); dst.pushTransformedChar('"'); break;
		case '\\': dst.pushTransformedChar('\\'); dst.pushTransformedChar('\\'); break;
		case '\b': dst.pushTransformedChar('\\'); dst.pushTransformedChar('b'); break;
		case '\f': dst.pushTransformedChar('\\'); dst.pushTransformedChar('f'); break;
		case '\n': dst.pushTransformedChar('\\'); dst.pushTransformedChar('n'); break;
		case '\r': dst.pushTransformedChar('\\'); dst.pushTransformedChar('r'); break;
		case '\t': dst.pushTransformedChar('\\'); dst.pushTransformedChar('t'); break;
		default:
			dst.pushTransformedChar(ch);
			break;
		};
	}
	dst.pushTransformedChar('\"');
};

template <class WriterType>
void Record :: writeAnything(WriterType& dst, FormatCalculations* fmt) const {
	switch (type) {
	case TYPE_STRING: WriteString(*accessString(), dst); break;
	case TYPE_WSTRING: WriteString(*accessWString(), dst); break;
	case TYPE_ARRAY: writeArray(dst, fmt); break;
	case TYPE_OBJECT: writeObject(dst, fmt); break;
	case TYPE_INT: writeInt(dst, fmt); break;
	case TYPE_FLOAT: writeFloat(dst, fmt); break;
	case TYPE_TRUE: dst.append("true"); break;
	case TYPE_FALSE: dst.append("false"); break;
	case TYPE_NULL: dst.append("null"); break;
	};
};

template <class WriterType>
void Record :: writeArray(WriterType& dst, FormatCalculations* fmt) const {
	assert(isArray());
	dst.pushTransformedChar('[');
	if(fmt->after_block_open_len) {
		dst.append(fmt->info.after_block_open);
	}
	auto arr = accessArray();
	++(fmt->pre_indent_total_cnt);
	fmt->pre_indent_total_len += fmt->pre_indent_len;
	for(size_t i = 0; i < arr->size(); ++i) {
		WritePreIndent(dst, fmt);
		(*arr)[i].writeAnything(dst, fmt);
		if(i != (arr->size() - 1)) {
			dst.pushTransformedChar(',');
			if(fmt->after_comma_len) {
				dst.append(fmt->info.after_comma);
			}
		}
		WriteAfterValue(dst, fmt);
	}
	fmt->pre_indent_total_len -= fmt->pre_indent_len;
	--(fmt->pre_indent_total_cnt);

	WritePreIndent(dst, fmt);
	dst.pushTransformedChar(']');
};

template <class WriterType>
void Record :: writeObject(WriterType& dst, FormatCalculations* fmt) const {
	assert(isObject());
	dst.pushTransformedChar('{');
	if(fmt->after_block_open_len) {
		dst.append(fmt->info.after_block_open);
	}

	auto obj_map = accessObjectMap();
	size_t countdown = obj_map->size();
	++(fmt->pre_indent_total_cnt);
	fmt->pre_indent_total_len += fmt->pre_indent_len;

	auto func = [&countdown, &dst, fmt](const std::string& key, const Record& value) {
		WritePreIndent(dst, fmt);
		WriteString(key, dst);
		if(fmt->around_colon_len) {
			dst.append(fmt->info.around_colon);
			dst.pushTransformedChar(':');
			dst.append(fmt->info.around_colon);
		} else {
			dst.pushTransformedChar(':');
		}
		value.writeAnything(dst, fmt);

		--countdown;
		if(countdown) {
			dst.pushTransformedChar(',');
			if(fmt->after_comma_len) {
				dst.append(fmt->info.after_comma);
			}
		}
		WriteAfterValue(dst, fmt);
	};
	obj_map->iterate(func);
	fmt->pre_indent_total_len -= fmt->pre_indent_len;
	--(fmt->pre_indent_total_cnt);

	WritePreIndent(dst, fmt);
	dst.pushTransformedChar('}');
};

template <class WriterType>
void Record :: writeInt(WriterType& dst, FormatCalculations* fmt) const {
	assert(isInt());
	char tmp[string_with_number_length_limit];
	snprintf(tmp, sizeof(tmp), "%ld", getInt());
	dst.append(tmp);
};

template <class WriterType>
void Record :: writeFloat(WriterType& dst, FormatCalculations* fmt) const {
	assert(isFloat());
	char tmp[string_with_number_length_limit];
	snprintf(tmp, sizeof(tmp), "%lf", double(getFloat()));
	dst.append(tmp);
};

// high level parsers, writers

std::string Record :: write(const FormatInfo& format) const {
	FormatCalculations fmt(format, sizeof(char));
	std::string result;
	result.reserve( estimate(&fmt) );

	StdStringWriter<std::string> writer(result);
	writeAnything(writer, &fmt);
	return result;
};

std::wstring Record :: writeW(const FormatInfo& format) const {
	FormatCalculations fmt(format, sizeof(wchar_t));
	std::wstring result;
	result.reserve( estimate(&fmt) );

	StdStringWriter<std::wstring> writer(result);
	writeAnything(writer, &fmt);
	return result;
};

void Record :: write(std::ostream& dst, const FormatInfo& format) const {
	FormatCalculations fmt(format, sizeof(char));
	StdOStreamWriter<std::ostream> writer(dst);
	writeAnything(writer, &fmt);
};

void Record :: writeW(std::wostream& dst, const FormatInfo& format) const {
	FormatCalculations fmt(format, sizeof(wchar_t));
	StdOStreamWriter<std::wostream> writer(dst);
	writeAnything(writer, &fmt);
};

Record Record :: Parse(const std::string& json_string, ParsingState* state) {
	StdStringReader<std::string, char> src(json_string);
	ParsingState local;
	return ParseAnything<char>(src, state ? state : &local);
};

Record Record :: Parse(const std::wstring& json_string, ParsingState* state) {
	StdStringReader<std::wstring, wchar_t> src(json_string);
	ParsingState local;
	return ParseAnything<wchar_t>(src, state ? state : &local);
};

Record Record :: Parse(std::istream& input_stream, ParsingState* state) {
	StdIStreamReader<std::istream, char> src(input_stream);
	ParsingState local;
	return ParseAnything<char>(src, state ? state : &local);
};

Record Record :: Parse(std::wistream& input_stream, ParsingState* state) {
	StdIStreamReader<std::wistream, wchar_t> src(input_stream);
	ParsingState local;
	return ParseAnything<wchar_t>(src, state ? state : &local);
};

}; // end njson
