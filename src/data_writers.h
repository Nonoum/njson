#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "unicode_helpers.h"

#include <string>
#include <ostream>

namespace njson {

template <class StringT> class StdStringWriter {
	StringT& str;
	typedef decltype(StringT()[0]) CharType;

public:
	StdStringWriter(StringT& _str) : str(_str) {
	};
	template <class CharT> void pushTransformedChar(CharT c) {
		if(sizeof(CharT) > sizeof(CharType)) {
			if(unicode::IsAboveAscii(c)) {
				char transformed[7];
				unicode::TransformChar(transformed, c);
				append(transformed);
			} else {
				str.push_back(c);
			}
		} else {
			str.push_back(c);
		}
	};
	template <class CharT> void append(const std::basic_string<CharT>& string) {
		for(size_t i = 0; string[i] != 0; ++i) {
			str.push_back(string[i]);
		}
	};
	void append(const StringT& string) {
		str.append(string);
	};
	void append(const char* string) {
		for(size_t i = 0; string[i] != 0; ++i) {
			str.push_back(string[i]);
		}
	};
};

template <> void StdStringWriter<std::string> :: append(const char* string) {
	str.append(string);
};

template <class StreamT> class StdOStreamWriter {
	StreamT& st;
	typedef typename StreamT::char_type CharType;

public:
	StdOStreamWriter(StreamT& _st) : st(_st) {
	};
	template <class CharT> void pushTransformedChar(CharT c) {
		if(sizeof(CharT) > sizeof(CharType)) {
			if(unicode::IsAboveAscii(c)) {
				char transformed[7];
				unicode::TransformChar(transformed, c);
				append(transformed);
			} else {
				st.put(c);
			}
		} else {
			st.put(c);
		}
	};
	template <class CharT> void append(const std::basic_string<CharT>& string) {
		for(size_t i = 0; string[i] != 0; ++i) {
			st.put(string[i]);
		}
	};
	void append(const std::basic_string<CharType>& string) {
		st.write(string.c_str(), string.length());
	};
	void append(const char* string) {
		for(size_t i = 0; string[i] != 0; ++i) {
			st.put(string[i]);
		}
	};
};

}; // end njson
