#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include <ctype.h>

namespace njson {

namespace unicode {

inline bool IsAboveAscii(wchar_t c) {
	return c > 127;
};

inline char HexadecimalToAscii(char value_0_15) {
	return value_0_15 < 10 ? '0' + value_0_15 : 'A' - 10 + value_0_15;
};

inline void TransformChar(char output[7], wchar_t c) {
	output[0] = '\\';
	output[1] = 'u';
	output[2] = HexadecimalToAscii((c >> 12) & 0x0f);
	output[3] = HexadecimalToAscii((c >> 8) & 0x0f);
	output[4] = HexadecimalToAscii((c >> 4) & 0x0f);
	output[5] = HexadecimalToAscii(c & 0x0f);
	output[6] = '\0';
};

template<class CharT>
inline int ExtractChar(const CharT* hex_number, int length, int& output) {
	output = 0;
	for(int i = 0; i < length; ++i) {
		output <<= 4;
		if(isdigit(hex_number[i])) {
			output |= hex_number[i] - '0';
		} else if(isalpha(hex_number[i])) {
			output |= hex_number[i] - (islower(hex_number[i]) ? 'a' : 'A') + 10;
		} else {
			return i;
		}
	}
	return length;
};

}; // end namespace unicode

}; // end njson
