#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../headers/utils.h"

#include <string.h>

namespace njson {

struct FormatCalculations {
	FormatInfo info;
	size_t after_block_open_len;
	size_t after_comma_len;
	size_t after_value_len;
	size_t around_colon_len;
	size_t pre_indent_len;

	size_t pre_indent_total_cnt;
	size_t pre_indent_total_len;

	const size_t output_char_size;

	FormatCalculations(const FormatInfo& _info, size_t out_char_size) : info(_info), output_char_size(out_char_size) {
		after_block_open_len = info.after_block_open ? strlen(info.after_block_open) : 0;
		after_comma_len = info.after_comma ? strlen(info.after_comma) : 0;
		after_value_len = info.after_value ? strlen(info.after_value) : 0;
		around_colon_len = info.around_colon ? strlen(info.around_colon) : 0;
		pre_indent_len = info.pre_indent ? strlen(info.pre_indent) : 0;

		pre_indent_total_cnt = 0;
		pre_indent_total_len = 0;
	};
};

}; // end njson
