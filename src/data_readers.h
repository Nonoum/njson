#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include <string>
#include <istream>
#include <assert.h>

namespace njson {

template <class StringT, class CharT> class StdStringReader {
	const CharT* data;
	const size_t length;
	size_t idx;
public:
	StdStringReader(const StringT& _str) : data(_str.c_str()), length(_str.length()), idx(0) {
	};
	bool get(CharT& output) {
		if(idx < length) {
			output = data[idx];
			++idx;
			return true;
		}
		return false;
	};
	void recover() {
		if(idx) {
			--idx;
		}
	};
	size_t pos() const {
		return idx;
	};
};

template <class StreamT, class CharT> class StdIStreamReader {
	StreamT& st;
	size_t idx;
	CharT last;
public:
	StdIStreamReader(StreamT& _st) : st(_st), idx(0), last(0) {
	};
	bool get(CharT& output) {
		if(st.get(output)) {
			last = output;
			++idx;
			return true;
		}
		return false;
	};
	void recover() {
		if(idx) {
			assert((last != 0) && "StdIStreamReader::recover used twice in a row");
			--idx;
			st.putback(last);
			assert((last = 0) || true); // assert to just skip this for release build
		}
	};
	size_t pos() const {
		return idx;
	};
};

}; // end njson
