#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "../headers/Record.h"
#include "unicode_helpers.h"

namespace njson {

static const char ascii_string_unicode_char_replacement = '#';

class AnyStringBuilder {
	StringType str;
	WStringType wstr;
	bool has_unicode;
public:
	AnyStringBuilder() : has_unicode(false) {};
	template <class CharType, class SourceType> bool onBackSlashU(SourceType& src, ParsingState* state) {
		CharType num_str[4];
		int parsed_char;
		if(src.get(num_str[0]) && src.get(num_str[1]) && src.get(num_str[2]) && src.get(num_str[3])) {
			const int length = sizeof(num_str)/sizeof(*num_str);
			const int n_good_chars = unicode::ExtractChar(num_str, length, parsed_char);
			if(n_good_chars != length) {
				state->setBadChar(src.pos() - length + n_good_chars, num_str[n_good_chars], TYPE_WSTRING);
				return false;
			}
		} else {
			state->setNoData(src.pos(), TYPE_WSTRING);
			return false;
		}
		onRegularChar<wchar_t>(parsed_char);
		return true;
	};
	template <class CharType> void onRegularChar(CharType ch) {
		has_unicode |= unicode::IsAboveAscii(ch);
		if(has_unicode) {
			if(str.length()) {
				wstr.reserve(str.length() + 2);
				wstr.append(std::begin(str), std::end(str));
				str.clear();
			}
			wstr.push_back(ch);
		} else {
			str.push_back(ch);
		}
	};
	DataType lastType() {
		return has_unicode ? TYPE_WSTRING : TYPE_STRING;
	};
	Record obtainResult() {
		Record rec(lastType());
		if(has_unicode) {
			rec.setWString(std::move(wstr));
		} else {
			rec.setString(std::move(str));
		}
		return rec;
	};
};

class AsciiStringBuilder {
	StringType str;
public:
	template <class CharType, class SourceType> bool onBackSlashU(SourceType& src, ParsingState* state) {
		str.push_back('\\');
		str.push_back('u');
		return true;
	};
	template <class CharType> void onRegularChar(CharType ch) {
		if(unicode::IsAboveAscii(ch)) {
			str.push_back(ascii_string_unicode_char_replacement);
		} else {
			str.push_back(ch);
		}
	};
	DataType lastType() {
		return TYPE_OBJECT; // this is supposed to be used for parsing object's keys, so this is expected type here
	};
	Record obtainResult() {
		Record rec(TYPE_STRING);
		rec.setString(std::move(str));
		return rec;
	};
};

}; // end njson
