This is a free JSON library for C++11.
Version: 1.0.3

Main documentation is placed in 'headers/njson.h'.
'headers/' dir contains all declarations of library's API, so it can be distributed separately from other code.

Dependencies: NONE, exception is that for building on Windows, 'make' will attempt using 'mkdir -p' command
	that will succeed only if there's unix's 'mkdir' in your environment path variable, having Git in path is enough though.

What's supported:
	- Unicode: supported, except keys of objects, since it's useless and inconvenient to have unicode keys. See details in "headers/Record.h"
	- Windows: MS Visual Studio, GCC
	- Linux: GCC, CLANG
	- Mac OS: GCC, CLANG
	*** adding more compiler/platform specifications in most cases is quite short and simple, see "build/" directory's contents.

Tested on:
	- Windows 7: MSVC 2013
	- Windows 8: MSVC 2013, GCC 4.8.1
	- Linux Fedora: GCC 4.8.3
	- Mac OS 10.9.5: CLANG

Quick sample usages for your estimation:
    auto my_json = njson::JObject("some int", njson::JInt(123),
                                  "some str", njson::JString("qwerty"),
                                  "an array", njson::JArray( njson::JInt(123), njson::JTrue(), njson::JBool(false) ),
                                  "false", njson::JFalse(),
                                  "float", njson::JFloat(123.456),
                                  "null", njson::JNull());
    if(my_json.contains("some str")) {
        // ...
    }
    if(my_json["an array"].isArray()) {
        // ...
    }
    auto int_property = my_json["some property"].asInt(100); // returns 100 if "some property" is not an integer

    auto some_array = my_json.get("some array", njson::JArray()); // adds and returns empty JArray() if there is no such key
    auto arr = my_json.atKey("an array"); // returns NULL if there is no such key
    if((arr != NULL) && arr->isArray()) {
        arr->pushBack( njson::JInt(5) );
    }
    my_json["new key"] = njson::JArray();
    auto moved_json = std::move(my_json);

    std::string json_str = moved_json.write(njson::FormatInfo::PrettyCRLF());

    if(moved_json.isObject()) {
        auto func = [](const std::string& key, const njson::Record& value) {
            std::cout << "key: " << key << ", value type: " << value.getType() << std::endl;
        };
        moved_json.iterate(func);
    }
    auto parsed_json = njson::ReadJson(json_str);

Build instructions:
	building library is provided via makefiles, see "build/" directory and choose it's sub-directory
	with abbreviation matching your compiler and platform (e.g. "win-msvc" is Windows + MS Visual Studio),
	every sub-directory in "build/" dir has it's "makefile" to execute for building or running tests.

	Also for Windows there is helper file "build/run-msvc2013-helper.bat" that opens MSVC 2013's command prompt
	from it's default installation path, you should execute makefile from this command prompt.

	Makefiles has same targets for all compiler/platform versions, targets are:
		"debug" - build debug version of library;
		"release" - build release version of library;
		"libs" - build both debug and release versions of library;
		"test-debug" - build and run tests for debug;
		"test-release" - build and run tests for release;
		"test" - perform both "test-debug" and "test-release";

	Sample build commands for Linux / Mac OS:
		cd build
		cd unix-gcc
		make test libs

	Sample build commands for Windows:
		cd build
		run-msvc2013-helper.bat
		cd win-msvc
		nmake test libs

	After successful build you'll get debug and release static libraries in "bin/debug/" and "bin/release/" respectively.
