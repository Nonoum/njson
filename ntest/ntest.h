#pragma once
/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include <vector>
#include <iostream>

namespace ntest {

class TestBase {
	const char* name;
	enum {
		INIT,
		SUCCESS,
		FAIL
	} result;
public:
	TestBase(const char* _name);
	bool run();
	const char* getName() const;

	static size_t RunTests(std::ostream& log_output); // returns count of fails
protected:
	static void PushRunner(TestBase*);

	void approve(bool condition); // don't use 'assert' name to not conflict with C-library
	virtual void runInternal() = 0;
};

}; //end ntest

#define NTEST(test_name) \
	class test_name : public ntest::TestBase { \
	public: \
		test_name() : ntest::TestBase(#test_name) { \
			PushRunner(this); \
		}; \
	protected: \
		void runInternal() override; \
	}; \
	static test_name test_name##_instance; \
	void test_name :: runInternal()
