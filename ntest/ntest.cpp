/*
Copyright (C) 2015 Evgeniy Evstratov

This file is part of njson.

njson is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 2.1 of the License, or
(at your option) any later version.

njson is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with njson.  If not, see <http://www.gnu.org/licenses/>.

See COPYING file for the full LGPL text.
*/

#include "ntest.h"

#include <algorithm>
#include <string.h>

namespace ntest {

typedef std::vector<TestBase*> RunnersType;

static RunnersType& GetRunners() {
	static RunnersType runners;
	return runners;
};

TestBase :: TestBase(const char* _name)
		: name(_name), result(INIT) {
};

bool TestBase :: run() {
	result = INIT;
	runInternal();
	return result == SUCCESS;
};

const char* TestBase :: getName() const {
	return name;
};

void TestBase :: PushRunner(TestBase* test) {
	auto comp = [](const TestBase* left, const TestBase* right) {
		return strcmp(left->getName(), right->getName()) < 0;
	};
	auto& tests = GetRunners();
	tests.insert(std::upper_bound(tests.begin(), tests.end(), test, comp), test);
};

size_t TestBase :: RunTests(std::ostream& log_output) {
	size_t n_failed = 0;
	size_t n_succeeded = 0;
	log_output << ":: Running tests...." << std::endl;
	for(auto runner : GetRunners()) {
		log_output << runner->getName() << " : ";
		bool result = false;
		try {
			result = runner->run();
			result ? ++n_succeeded : ++n_failed;
		} catch (...) {
			++n_failed;
		}
		log_output << (result ? "success" : "fail") << std::endl;
	}
	log_output << ":: Total tests : " << (n_failed + n_succeeded) << std::endl;
	log_output << ":: Succeeded : " << n_succeeded << std::endl;
	log_output << ":: Failed : " << n_failed << std::endl;
	return n_failed;
};

void TestBase :: approve(bool condition) {
	if(result != FAIL) {
		result = condition ? SUCCESS : FAIL;
	}
};

}; // end ntest
